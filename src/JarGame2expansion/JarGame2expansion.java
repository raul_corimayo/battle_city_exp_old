package JarGame2expansion;

import JarGame2expansion.Enemies.Item;
import JarGame2expansion.net.ClientInstance;
import JarGame2expansion.net.GameStatusPacket;
import JarGame2expansion.net.ServerInstance;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.BindException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import org.gnet.client.GNetClient;
import org.gnet.client.ServerModel;
import org.gnet.server.GNetServer;

public class JarGame2expansion extends JComponent implements KeyListener{

//Constantes------------------------------------------------------------------//
    private final static int ANCHO = 1300;
    private final static int ALTO = 700;
//Atributos-------------------------------------------------------------------//
    private Image imgEscenario,  imgExplosiones,  imgCursor,  imgPresentando;
    private Image[] imgPlayer = new Image[2];//max players = 2
    private Image[] imgEnemyBlind = new Image[5]; //max enemysB = 4 + 1(red)
    private Image[] imgEnemyNoBlind = new Image[5];//max enemysNoB = 4 + 1(red)
    private Image imgKeyboard;
    GestorImagenes gi = new GestorImagenes();
    GestorSonido gs = new GestorSonido();
    Pantalla pantalla;
    boolean loadConstruction = false;
    int hiScore = 0;
    //--------------posiblemente variables auxiliares comunes a todo estado
    int t = 0;//var de proposito gral
    boolean flag = false;//var de proposito gral
    int x = 0;//var de proposito gral
    int y = 0;//var de proposito gral
    int opc = 0;
    int stage;//stage actual
    int stageFinal = 25;
    //estructuras para escenario
    byte matrizDimension = 26;
    byte[][] matriz = new byte[matrizDimension][matrizDimension];
    //----------ED StageJugando---------
    //ed players
    int nPlayers;//seteada  en opcionSeleccionando
    Player[] players = new Player[2];//final int MaxNumPlayers = 2;
    int aguilaX = 12, aguilaY = 24, aguilaEstado = 0;//0:viva 1:muerta
    Explosiones exp = new Explosiones();
    GestionDañoLadrillos gDL = new GestionDañoLadrillos();
    GestionDañoPiedras gDP = new GestionDañoPiedras();
    Enemies enemys = new Enemies();
    boolean pause = false;
    boolean mostrarPause = false;
    int tPause = 0;
    float tBegin = 0;
    boolean begin = false;
    //Stage Ganando/Stage Perdiendo
    int tpo = 0;
    //Puntuando //*nota usa sleep(n); (n!=20)!!!
    int tT = 0;
    int[] countTTP1 = new int[4];//4 = tipos de tanques enemigos
    int[] countTTP2 = new int[4];//4 = tipos de tanques enemigos
    int[] newScore = new int[2];//2 = nPlayers
    //load stage
    int loadStage;
    byte loadTimeEntreT;
    byte loadEnemySimul;
    byte[] loadTiposT;
    int loadOpc;
    boolean loadNumberValid;
    int loadIndex;
    GestorEscenarios ge = new GestorEscenarios(matrizDimension);
    //demostrando
    boolean demostrando;
    private int kx = 2;
    private int ky = 2;
    
    //--net--
    String mensajeNet;
    ClientInstance clientInstance;
    boolean onNet = false;
    int idPlayer = 0;
    
    public static final int PORT = 43594;
    public static String host = "localhost";
    public static int spf = 15;
    private JFrame jf;
    
    //options
    private String[] difficulty = {"Very easy","Easy", "Medium", "Hard", "Very hard"};
    private String[] speed = {"Slower", "Slow", "Medium", "Fast", "Faster"};
    private int[] speedSleep = {120,60,30,20,10};
    private int indexDifficulty = 2;
    private int indexSpeed = 2;
    private boolean useCustomMaps;
    
//Metodos---------------------------------------------------------------------//
    

    public JarGame2expansion(int kx, int ky) {
        this.kx = kx;
        this.ky = ky;
        //cada pantalla deberia tener su funcion mostrar...
        //inicializacion_del_juego () ;  //=+- inicializarGame();
        inicializarGame();
        setPreferredSize(new Dimension(ANCHO, ALTO));
        
        JTextField jt = new JTextField();
        this.add(jt);
        jt.addKeyListener(this);
        
//        // esto debe ir si quiero que escuche al mouse
//        this.addMouseListener(new java.awt.event.MouseListener() {
//            public void mouseClicked(MouseEvent e) {
//                System.out.println(e.getX()+";"+e.getY());
//            }
//            public void mousePressed(MouseEvent e) {
//            }
//            public void mouseReleased(MouseEvent e) {
//            }
//            public void mouseEntered(MouseEvent e) {
//            }
//            public void mouseExited(MouseEvent e) {
//            }
//        });

            
    }

    private void dibuja() throws Exception {
        SwingUtilities.invokeAndWait(new Runnable() {

            public void run() {
                paintImmediately(0, 0, ANCHO*kx/4, ALTO*ky/3);
            }
        });
    }

    public void main(JFrame jf) throws Exception {
        this.jf = jf;
        while (true) {
            controles();
            presentando();//presentando stage=1;
            do {
                pantalla = opcionSeleccionando();//modifica el valor de pantalla
                if (pantalla == Pantalla.construyendo) {
                    construyendo();
                    saveStage();
                } else if (pantalla == Pantalla.demostrando) {
                    demostrando();
                } else if (pantalla == Pantalla.opciones) {
                    opciones();
                } else if (pantalla == Pantalla.net){
                    net();//modifica el valor de pantalla
                    onNet = true;
                }
            } while (pantalla != Pantalla.stageSeleccionando);
            stageSeleccionando();
            boolean stageWin;
            do {
                stagePresentando();
                stageWin = stageJugando();
                if (stageWin) {
                    stageGanando();
                    puntuando();
                }//puntuando, al final, incrementa stage
                else {
                    stagePerdiendo();
                    puntuando();
                    gameOver();
                    recordMostrando();
                }
            } while (stageWin);
        }

    }
    
    @Override
    public void paint(Graphics g) {
        
        switch (pantalla) {
            case controles:
                mostrarControles(g);
                break;
            case presentando:
                mostrarPresentando(g);
                break;
            case opcionSeleccionando:
                mostrarOpcionSeleccionando(g);
                break;
            case stageSeleccionando:
                mostrarStageSeleccionando(g);
                break;
            case stagePresentando:
                mostrarStagePresentando(g);
                break;
            case construyendo:
                mostrarConstruyendo(g);
                break;
            case stageJugando:
                mostrarStageJugando(g);
                break;
            case stageGanando:
                mostrarStageGanando(g);
                break;
            case stagePerdiendo:
                mostrarStagePerdiendo(g);
                break;
            case puntuando:
                mostrarPuntuando(g);
                break;
            case demostrando:
                mostrarDemostrando(g);
                break;
            case recordMostrando:
                mostrarRecordMostrando(g);
                break;
            case saveStage:
                mostrarSaveStage(g);
                break;
            case gameOver:
                mostrarGameOver(g);
                break;
            case net:
                mostrarNet(g);
                break;
            case opciones:
                mostrarOpciones(g);
                break;
        }
    }

    private void controles() throws Exception {
        System.out.println("controles");
        pantalla = Pantalla.controles;
        t = 0;
        while (players[0].cm.teclaPulsada()) {
            Thread.sleep(20);
        }
        do {
            dibuja();
            Thread.sleep(20);
            t++;
        } while (!players[0].cm.teclaPulsada());
    }
    
    private void presentando() throws Exception {
        System.out.println("presentando");
        pantalla = Pantalla.presentando;
        while (players[0].cm.teclaPulsada()) {
            Thread.sleep(20);
        }
        gs.playFondo(gs.PRESENTAC);
        stage = 1;
        do {
            dibuja();
            Thread.sleep(20);
            t++;
        } while (t < 250 && !players[0].cm.teclaPulsada());
        int timeWait = 0;
        do {
            dibuja();
            Thread.sleep(20);
            timeWait++;
        } while (!players[0].cm.teclaPulsada() && (timeWait < 600));
        gs.stopFondo();
    }

    private Pantalla opcionSeleccionando() throws Exception {
        System.out.println("opcionSeleccionando");
        players[0].cm.falseAll_menosStart();
        opc = 0;
        pantalla = Pantalla.opcionSeleccionando;
        boolean toggle = true;
        int timeDead = 0;//tiempo transcurrido (en unidades del sleep)
        //sinque se presione up, down, start
        gs.inicializar();
        boolean soltoStart = false;
        do {
            dibuja();
            Thread.sleep(20);
            timeDead++;
            if (toggle) {
                //up down presionados
                if (players[0].cm.up) {
                    gs.play(gs.COIN);
                    if (opc < 1) {
                        opc = 3;
                        //opc = 2;
                    } else {
                        opc--;
                    }
                    toggle = false;
                } else if (players[0].cm.down) {
                    gs.play(gs.COIN);
                    if (opc > 2) {
                    //if (opc > 1) {
                        opc = 0;
                    } else {
                        opc++;
                    }
                    toggle = false;
                }
            } else {
                if (!players[0].cm.up && !players[0].cm.down) {
                    //up down liberados (se sualtan los botones direccionales en cuestion)
                    toggle = true;
                    timeDead = 0;
                }
            }
            if (!players[0].cm.start) {
                soltoStart = true;
            }
        } while (timeDead < 500 && !players[0].cm.start || !soltoStart);

        Pantalla aux;
        demostrando = false;
        if (timeDead >= 200) {//timerTank out! --> DemostrarGame
            demostrando = true;
            nPlayers = 2;
            aux = Pantalla.demostrando;
        } else if (opc < 2) {//opc=0->1player, opc=1-> 2players
            aux = Pantalla.stageSeleccionando;
            nPlayers = opc + 1;//solo puede tomar valores 0 o 1,
        } else if (opc == 2){//opc=2-->construction
            aux = Pantalla.construyendo;
        } else if (opc == 3){//opc=3-->opciones
            aux = Pantalla.opciones;
        } else {//if (opc == 4)
            aux = Pantalla.net;
            nPlayers = 2;
        }
        //ya que 2 es para construccion, y 3,4,5,... no existen
        players[0] = new Player();
        players[1] = new Player();
        //si esta demostrando, los dos playerIA juegan la demostracion.
        if (nPlayers < 2 && !demostrando) {
            //se elimina player2 si se seleccionó 1 player y no esta demostrando
            players[1].vidas = -1;
        }

        aguilaEstado = 0;
        gs.play(gs.SELECTION);
        Thread.sleep(500);
        return aux;
    }

    private void stageSeleccionando() throws Exception {
        System.out.println("stageSeleccionando");
        pantalla = Pantalla.stageSeleccionando;
        int timeDead = 0;//tpoEntreNacimiento transcurrido (en unidades del sleep)
        //sinque se presione buttonA, buttonB, play
        boolean soltoStart = false;
        loadEscenarioAndSetMatriz(stage);
        do {
            dibuja();
            Thread.sleep(50);
            timeDead++;
            if (players[0].cm.buttonA) {
                if (stage < stageFinal) {
                    stage++;
                    loadEscenarioAndSetMatriz(stage);
                    gs.play(gs.COIN);
                }
                timeDead = 0;
                players[0].cm.buttonA = false;
            } else if (players[0].cm.buttonB) {
                if (1 < stage) {
                    stage--;
                    loadEscenarioAndSetMatriz(stage);
                    gs.play(gs.COIN);
                }
                timeDead = 0;
                players[0].cm.buttonB = false;
            } else if (players[0].cm.down || players[0].cm.left || players[0].cm.right || players[0].cm.up) {
                useCustomMaps = !useCustomMaps;
                loadEscenarioAndSetMatriz(stage);
                gs.play(gs.COIN);
                timeDead = 0;
                players[0].cm.down = false;
                players[0].cm.left = false;
                players[0].cm.right = false;
                players[0].cm.up = false;
            }
            if (!players[0].cm.start) {
                soltoStart = true;
            }
        } while ((timeDead < 200 && !players[0].cm.start) || (!soltoStart));
    }
    private void loadEscenarioAndSetMatriz(int stage){
        try {
            if(ge.loadEscenario(stage, useCustomMaps)){
                matriz = ge.escenario.matriz26;//***
            }else{
                matriz = new byte[ge.escenario.matriz26.length][ge.escenario.matriz26.length];
            }
        } catch (IOException ex) {
            Logger.getLogger(JarGame2expansion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JarGame2expansion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void stagePresentando() throws Exception {
        System.out.println("stagePresentando");
        pantalla = Pantalla.stagePresentando;
        gs.play(gs.CAMPANA);
        t = 225;
        do {
            dibuja();
            Thread.sleep(10);
            t--;
        } while (0 < t);
        gs.stopFondo();
        Thread.sleep(100);
    }

    private void construyendo() throws Exception {
        System.out.println("construyendo");
        pantalla = Pantalla.construyendo;
        ge.escenario = new Escenario(26);
        inicializarEnemys();//para obtener valores de Cantidad de enemigos
        //matriz = ge.escenario.matriz26;
        loadConstruction = true;
        x = 0;
        y = 0;
        byte type = 0;
        int time = 0;
        boolean changeType = true;
        boolean soltoStart = false;
        matriz[11][23] = 1;
        matriz[11][24] = 1;
        matriz[11][25] = 1;
        matriz[14][23] = 1;
        matriz[14][24] = 1;
        matriz[14][25] = 1;
        matriz[12][23] = 1;
        matriz[13][23] = 1;
        do {
            time++;
            if (14 < time) {
                flag = !flag;
                time = 0;
            }
            dibuja();
            Thread.sleep(20);
            int lim = matrizDimension - 1;
            if (players[0].cm.up) {
                players[0].cm.up = false;
                if (0 < y) {
                    y--;
                    changeType = false;
                }
            } else if (players[0].cm.down) {
                players[0].cm.down = false;
                if (y < lim) {
                    y++;
                    changeType = false;
                }
            } else if (players[0].cm.left) {
                players[0].cm.left = false;
                if (0 < x) {
                    x--;
                    changeType = false;
                }
            } else if (players[0].cm.right) {
                players[0].cm.right = false;
                if (x < lim) {
                    x++;
                    changeType = false;
                }
            } else if (players[0].cm.buttonA) {
                players[0].cm.buttonA = false;
                if (changeType) {
                    if (type < 6) {
                        type++;
                    } else {
                        type = 0;
                    }
                }
                asignarMatriz(type);
                changeType = true;
            } else if (players[0].cm.buttonB) {
                players[0].cm.buttonB = false;
                if (changeType) {
                    if (0 < type) {
                        type--;
                    } else {
                        type = 6;
                    }
                }
                asignarMatriz(type);
                changeType = true;
            }

            if (!players[0].cm.start) {
                soltoStart = true;
            }

        } while (!players[0].cm.start || !soltoStart);
    }

    public void asignarMatriz(byte ty) {//nada,lad,hier,pas,ag,cal,pied
        if (x != aguilaX || y != aguilaY) {
            matriz[x][y] = ty;
        }
    }

    public void saveStage() throws Exception {
        System.out.println("saveStage");
        pantalla = Pantalla.saveStage;
        boolean soltoStart = false;
        loadIndex = 0;
        loadOpc = 0;
        loadStage = 1;
        loadEnemySimul = 1;
        loadTiposT = new byte[enemys.maxEnemysForLevel];
        loadTimeEntreT = 100;
        validarNumberStage(loadStage);
        do {
            players[0].cm.falseAll_menosStart();
            dibuja();
            Thread.sleep(50);
            if (players[0].cm.up) {
                if (0 < loadOpc) {
                    loadOpc--;
                }
            } else if (players[0].cm.down) {
                if (loadOpc < 4) {
                    loadOpc++;
                }
            }
            switch (loadOpc) {
                case 0:
                    if (players[0].cm.buttonA) {
                        if (loadStage < stageFinal) {
                            loadStage++;
                            validarNumberStage(loadStage);
                        }
                    } else if (players[0].cm.buttonB) {
                        if (1 < loadStage) {
                            loadStage--;
                            validarNumberStage(loadStage);
                        }
                    }
                    break;
                case 1:
                    if (players[0].cm.left) {
                        if (0 < loadIndex) {
                            loadIndex--;
                        }
                    } else if (players[0].cm.right) {
                        if (loadIndex < enemys.maxEnemysForLevel - 1) {
                            loadIndex++;
                        }
                    } else if (players[0].cm.buttonA) {
                        if (loadTiposT[loadIndex] < 3) {
                            loadTiposT[loadIndex]++;
                        }
                    } else if (players[0].cm.buttonB) {
                        if (-1 < loadTiposT[loadIndex]) {
                            loadTiposT[loadIndex]--;
                        }
                    }
                    break;
                case 2:
                    if (players[0].cm.buttonA) {
                        if (loadTimeEntreT < 200) {
                            loadTimeEntreT++;
                        }
                    }//tpo entre t: max=200;min=0;
                    else if (players[0].cm.buttonB) {
                        if (0 < loadTimeEntreT) {
                            loadTimeEntreT--;
                        }
                    }
                    break;
                case 3:
                    if (players[0].cm.buttonA) {
                        if (loadEnemySimul < enemys.maxEnemysSimul) {
                            loadEnemySimul++;
                        }
                    }// max=20;min=1;
                    else if (players[0].cm.buttonB) {
                        if (1 < loadEnemySimul) {
                            loadEnemySimul--;
                        }
                    }
                    break;
            }
            if (!players[0].cm.start) {
                soltoStart = true;
            }
        } while (!players[0].cm.start || !soltoStart);
        if (loadOpc == 4) {
            ge.saveCustomEscenario(loadStage, loadEnemySimul, loadTimeEntreT, loadTiposT, matriz);
            gs.play(gs.SELECTION);
            loadConstruction = false;
        } else {
            gs.play(gs.STEP);
        }
    }
    private void validarNumberStage(int loadStage) throws IOException, ClassNotFoundException{
        loadNumberValid = ! ge.existeCustomMapa(loadStage);
    }

    private boolean stageJugando() throws Exception {
        System.out.println("stageJugando");
        pantalla = Pantalla.stageJugando;
        inicializarPlayers(nPlayers);
        enemys = null;
        enemys = new Enemies();
        ge.loadEscenario(stage, useCustomMaps);
        inicializarEnemys();
        inicializarMatriz(stage, loadConstruction);//*
        gDL.inicializar(matriz);
        gDP.inicializar(matriz);
        loadConstruction = false;//**
        //* si loadConstruction es true , cargara la construccion hecha
        //**construction sera true solo para la primera vez (el primer stage)
        gs.playFondo(gs.PROBLEM_2);
        t = 0;
        x = 0;//necesarios para mostrarStageJugando()
        tBegin = 1.01f;
        begin = true;//neblina del comienzo del stage = true
        boolean win = false;//winStage
        boolean lose = false;//loseStage
        int countEnd = 200;//tiempo despues de ganar o perder
        boolean endStage = false;
        float t0 = System.nanoTime();
        float tf ;
        
        
        do {
            //Runtime.getRuntime().gc();
//          //BEGIN INFO FRAMES
            tf = System.nanoTime();
            //System.out.println("fps= "+1000000000/(tf-t0));
            //jf.setTitle(Main.TITLE+" fps= "+1000000000/(tf-t0));
            t0 = tf;
//          //END INFO FRAMES
            dibuja();
            Thread.sleep(speedSleep[indexSpeed]);
            //begin:
            if (begin) {
                tBegin = (float) Math.pow(tBegin, 1.1);
                if (2 < tBegin) {
                    begin = false;
                }
            }
            
            // gestion items
            for (int ne = 0; ne < enemys.vecEnem.length; ne++) {
                if (enemys.vecEnem[ne] != null) {
                    //ne = enemys (2,3,4,...)
                    Tanque t = enemys.vecEnem[ne];
                    byte id = (byte) (ne+2);
                    if (t.timeRemainingNaciendo < 1 && ((1 < id) || aguilaEstado == 0)) {
                        if (1 < id || players[id].tiempoRestanteEfReloj < 1) {
                            gestionItems(t,id);
                        }
                    }
                }
            }
            //players
            for (byte np = 0; np < nPlayers; np++) {
                //np = id: players (0,1)
                if (players[np].vivo()) {
                    Tanque t = players[np];
                    byte id = np;
                    if (t.timeRemainingNaciendo < 1 && ((1 < id) || aguilaEstado == 0)) {
                        if (1 < id || players[id].tiempoRestanteEfReloj < 1) {
                            gestionItems(t,id);
                        }
                    }
                }
            }
            
          // actualizar cms players y de enemies
            //players
            for (byte np = 0; np < nPlayers; np++) {
                //np = id: players (0,1)
                if (players[np].vivo()) {
                    if(onNet){
                        
                    }else if (demostrando) {
                        players[np].IA(enemys);
                    }
                }
            }
            //enemies
            for (int ne = 0; ne < enemys.vecEnem.length; ne++) {
                if (enemys.vecEnem[ne] != null) {
                    if(!onNet || idPlayer == 1){
                        enemys.IA(ne, players, stage, indexDifficulty);
                    }
                }
            }
            if(onNet){
                // sync cms (e items)
                // crear model
                GameStatusPacket status = new GameStatusPacket();
                if(idPlayer == 1){
                    status.cm1 = players[0].cm;
                    status.cme = enemys.getVecMCs();
                    for(Item it : enemys.listaItemsCreados){
                        enemys.addItemNuevo(it);
                    }
                    enemys.vaciarListaItemsCreados();
                    status.ic = enemys.item;
                }else{
                    status.cm2 = players[1].cm;
                }
                status.idPlayer = idPlayer;
                
                // enviar model
                clientInstance.sendStatus(status);
                // recibir model y bloquear
                status = clientInstance.receiveStatus();
                players[0].cm = status.cm1;
                players[1].cm = status.cm2;
                players[0].cm.start = false;
                players[1].cm.start = false;
                enemys.setVecMCs(status.cme);
                enemys.item = status.ic;
            }
            
            
          // gestionar tanques
            //enemies
            for (int ne = 0; ne < enemys.vecEnem.length; ne++) {
                if (enemys.vecEnem[ne] != null) {
                    //ne = enemys (2,3,4,...)
                    gestionarTanque(enemys.vecEnem[ne],(byte) (ne + 2));
                }
            }
            //players
            for (byte np = 0; np < nPlayers; np++) {
                //np = id: players (0,1)
                if (players[np].vivo()) {
                    gestionarTanque(players[np], np);
                }
            }
            
            
            //items (algunos)
            enemys.gestionReloj();
            gDL.gestionPala(matriz);
            //enemys
            enemys.gestionarNacimientosYMuertes(stage);
            
            
            //pausa
            if (players[0].cm.start) {
                players[0].cm.start = false;
                pause = true;
                mostrarPause = true;
                tPause = 0;
                while (pause) {
                    if (players[0].cm.start) {
                        players[0].cm.start = false;
                        pause = false;
                    }
                    //como esta dentro del bucle es como si estuviera en otra
                    // pantalla, por eso necesita dibja() y Thread.sleep(20).
                    dibuja();
                    Thread.sleep(20);
                    if (demostrando) {
                        //seteo de condiciones de salida inmediata y fin de fondo
                        countEnd = 0;
                        win = true;
                        pause = false;
                        gs.stopFondo();
                    }
                }
            }
            //end stage?
            if (!endStage) {
                if ((!players[0].vivo() && !players[1].vivo()) || aguilaEstado == 1) {
                    lose = true;
                    endStage = true;
                    gs.stopFondo();
                }
                if (enemys.endEnemys()) {
                    win = true;
                    endStage = true;
                    gs.stopFondo();
                }
            } else {
                countEnd--;
            }
            
            if(onNet){
                //---load 
                players[0].cm.up = players[0].cmExtra.up;
                players[0].cm.down = players[0].cmExtra.down;
                players[0].cm.left = players[0].cmExtra.left;
                players[0].cm.right = players[0].cmExtra.right;
                players[0].cm.buttonA = players[0].cmExtra.buttonA;
                players[0].cm.buttonB = players[0].cmExtra.buttonB;
                players[1].cm.up = players[1].cmExtra.up;
                players[1].cm.down = players[1].cmExtra.down;
                players[1].cm.left = players[1].cmExtra.left;
                players[1].cm.right = players[1].cmExtra.right;
                players[1].cm.buttonA = players[1].cmExtra.buttonA;
                players[1].cm.buttonB = players[1].cmExtra.buttonB;
            }
                
        } while (!win && !lose || 0 < countEnd);
        if (aguilaEstado == 1) {
            lose = true;
        }
        return !lose;
    }

    public void inicializarMatriz(int stage, boolean loadConstruction) throws IOException, ClassNotFoundException {
        if (!loadConstruction) {
            matriz = ge.getMatriz26();
        }
    }

    public void inicializarEnemys() throws IOException, ClassNotFoundException {
        enemys.nVTTE = 0;
        enemys.nextPosicEnemy = 0;
        enemys.maxEnemysSimul = ge.getMaxEnemySimul();
        enemys.maxEnemysForLevel = ge.getMaxEnemysForLevel();
        enemys.tpoEntreNacimiento = ge.getTpoEntreNacimiento();
        enemys.vectorTipoTanquesEnemy = ge.getVectorTipoTanquesEnemy();
        enemys.vecEnem = new Tanque[enemys.maxEnemysSimul];
        enemys.timerTank = new int[enemys.maxEnemysSimul];
        enemys.timer2Tank = new int[enemys.maxEnemysSimul];
        enemys.rTank = new int[enemys.maxEnemysSimul];
        enemys.countTpoEntreNac = enemys.tpoEntreNacimiento;
    }

    public void inicializarPlayers(int numPlayers) {
        inicializarPosicPlayer(0);
        inicializarBalasPlayer(0);
        inicializarDirPlayer(0);
        if (1 < numPlayers) {
            inicializarPosicPlayer(1);
            inicializarBalasPlayer(1);
            inicializarDirPlayer(1);
        }
    }

    public void inicializarPosicPlayer(int id) {
        if (0 == id) {
            players[0].px = 4 * 16;
            players[0].py = 12 * 16;
        } else {
            players[1].px = 8 * 16;
            players[1].py = 12 * 16;
        }
    }

    public void inicializarDirPlayer(int id) {
        if (0 == id) {
            players[0].dir = 0;
        } else {
            players[1].dir = 0;
        }
    }

    public void inicializarBalasPlayer(int id) {
        players[id].nBalasEnCurso = 0;
    }

    public boolean pasaTanqueM(int xx, int yy) {
        // xx, yy posiciones de matriz por las que intentara pasar
        //detecta choques con escenario y aguila
        if (-1 < yy && -1 < xx && xx < matrizDimension && yy < matrizDimension) {
            boolean chocaConAguila = false;
            if ((aguilaX == xx || aguilaX + 1 == xx) && (aguilaY == yy || aguilaY + 1 == yy)) {
                chocaConAguila = true;
            }
            int m = matriz[xx][yy];
            return !(m == 1 || m == 2 || m == 4 || m == 6 || chocaConAguila);
        } else {
            return true;
        }
    }

    public boolean pasaBalaM(int xx, int yy) {
        if (-1 < yy && -1 < xx && xx < matrizDimension && yy < matrizDimension) {
            boolean chocaConAguila = false;
            if ((aguilaX == xx || aguilaX + 1 == xx) && (aguilaY == yy || aguilaY + 1 == yy)) {
                chocaConAguila = true;
                aguilaEstado = 1;
            }
            int m = matriz[xx][yy];
            return !(m == 1 || m == 2 || m == 6 || chocaConAguila);
        } else {
            return true;
        }
    }

    public boolean tanqueChocaConTanques(Tanque tan, int id, int dir) {
        //INDICA SI CHOCA CON OTROS TANQUES (incluye a todos los tanques)
        //int id: players (0,1), enemys (2,3,4,...)
        //int dir : direccion en que se pretende desplazar
        boolean choca = false;
        int dX = 0;//variacion en pX
        int dY = 0;//variacion en pY
        switch (dir) {
            case 0:
                dY = -tan.vel;
                break;//arriba
            case 1:
                dY = +tan.vel;
                break;//abajo
            case 2:
                dX = -tan.vel;
                break;//izquierda
            case 3:
                dX = +tan.vel;
                break;//derecha
        }
        int newX = tan.px + dX;//posicion en x a evaluar
        int newY = tan.py + dY;//posicion en y a evaluar
        //choca con players ?
        for (int np = 0; np < 2; np++) {
            if (!choca && id != np && players[np].vivo()) {
                choca = chocan(newX, newY, players[np].px, players[np].py, dir, 16);
            }
        }
        //choca con enemys?
        int ne = 0;
        while (ne < enemys.vecEnem.length && !choca) {
            if ((id != ne + 2) && (enemys.vecEnem[ne] != null)) {//id de los enemis (2,3,4,...[i+2],...)
                choca = chocan(newX, newY, enemys.vecEnem[ne].px, enemys.vecEnem[ne].py, dir, 16);
            }
            ne++;
        }
        return choca;
    }

    public boolean chocan(int x, int y, int xx, int yy, int dir, int tamaño) {
        //x,y,xx,yy : posiciones de prueba x,y de los tanques (x:1, xx:2)
        //dir direccion de movimiento del tanque 1
        int dx = x - xx;
        int dy = y - yy;
        boolean aux = false;
        if (-tamaño < dx && dx < tamaño && -tamaño < dy && dy < tamaño) {//16 = tamaño del tanque
            //(dx<16 && -16<dx) choque en x, analogo para choque en y
            //si chocaron simultaneamente en x e y:
            if ((dx < 0 && dir == 3) || (0 < dx && dir == 2) || (0 < dy && dir == 0) || (dy < 0 && dir == 1)) {
                //si choco y esta en direccion a seguir chocando --> no avanza
                // en otro caso si(si choco pero desea separarse)
                aux = true;
            }
        }
        return aux;
    }

    public boolean balaChocaConTanques(int nb, Tanque tan, int id, int dir, int daño) {
        //NOTA toma una sola bala---> debe ser llamado por cada bala nb
        //detecta si la bala choco con algun tanque, ademas elimina al tanque muerto
        //int id: players (0,1), enemys (2,3,4,...)
        //int dir : direccion en que se pretende desplazar
        boolean choca = false;

        int newX = tan.balasEnCurso[nb].px - 6;
        int newY = tan.balasEnCurso[nb].py - 6;
        //choca con players ?
        int np = 0;
        while (np < 2 && !choca) {
            if (id != np && players[np].vivo() && players[np].timeRemainingNaciendo < 1) {
                choca = chocan(newX, newY, players[np].px, players[np].py, dir, 10);
                if(id<2){choca = false;}// las balas de los players se traspasan entre si
                if (choca && 1 < id && !players[np].escudoActivado) {//bala choca con player
                    players[np].blindaje = players[np].blindaje - daño;
                    if (players[np].blindaje < 1) {
                        matarPlayer(np);
                    }
                }
            }
            np++;
        }
        //choca con enemys?
        int ne = 0;
        while (ne < enemys.vecEnem.length && !choca) {
            if ((id != ne + 2) && (enemys.vecEnem[ne] != null) && enemys.vecEnem[ne].timeRemainingNaciendo < 1) {
                //id de los enemis (2,3,4,...[i+2],...)
                choca = chocan(newX, newY, enemys.vecEnem[ne].px, enemys.vecEnem[ne].py, dir, 10);
                if (choca && id < 2 && !enemys.vecEnem[ne].escudoActivado) {
                    enemys.vecEnem[ne].blindaje = enemys.vecEnem[ne].blindaje - daño - players[id].level;
                    if (enemys.vecEnem[ne].blindaje < 1) {
                        //muere enemy
                        gs.play(gs.EXP_TANK);
                        exp.add(1, enemys.vecEnem[ne].px + 8, enemys.vecEnem[ne].py + 8);
                        players[id].addTanqueVencidoTipo(enemys.vecEnem[ne].tipo);
                        enemys.deadTank(ne, onNet);
                    }
                }
            }
            ne++;
        }
        return choca;
    }

    public boolean balaChocaConBalas(int nb, Tanque tan, int id, int dir) {
        //detecta si chocaron dos balas ademas elimina la bala con la que choco!
        //nb:numero de bala, id:id del tanque
        boolean choca = false;
        if(tan.balasEnCurso[nb].tipo==2){return false;}//si es laser no puede chocar con ninguna bala..!!!
        int newX = tan.balasEnCurso[nb].px;
        int newY = tan.balasEnCurso[nb].py;
        //choca con balas de players ?
        int np = 0;
        while (np < 2 && !choca) {
            if (id != np) {
                int ib = 0;
                while (ib < players[np].nBalasEnCurso && !choca) {
                    choca = chocan(newX, newY, players[np].balasEnCurso[ib].px, players[np].balasEnCurso[ib].py, dir, 8);
                    if(players[np].balasEnCurso[ib].tipo==2){choca=false;}//una bala enemiga no choca con el laser de un player
                    if (choca && players[np].balasEnCurso[nb] != null) {
                        exp.add(0, players[np].balasEnCurso[nb].px, players[np].balasEnCurso[nb].py);
                    }
                    ib++;
                }
            }
            np++;
        }
        //choca con balas de enemys?
        int ne = 0;
        while (ne < enemys.vecEnem.length && !choca) {
            if ((id != ne + 2) && (enemys.vecEnem[ne] != null)) {//id de los enemis (2,3,4,...[i+2],...)
                int ib = 0;
                while (ib < enemys.vecEnem[ne].nBalasEnCurso && !choca) {
                    choca = chocan(newX, newY, enemys.vecEnem[ne].balasEnCurso[ib].px, enemys.vecEnem[ne].balasEnCurso[ib].py, dir, 8);
                    if (choca && enemys.vecEnem[ne] != null && enemys.vecEnem[ne].balasEnCurso[nb] != null) {
                        exp.add(0, enemys.vecEnem[ne].balasEnCurso[nb].px, enemys.vecEnem[ne].balasEnCurso[nb].py);
                    }
                    ib++;
                }
            }
            ne++;
        }
        return choca;
    }

    public void gestionarTanqueMov(Tanque t, byte id) {
        //
        //player
        //
        //movimiento
        int v = t.vel;
        int px = t.px;
        int py = t.py;
        int xx = px / 8, yy = py / 8;
        int xx2 = (px + 7) / 8, yy2 = (py + 7) / 8;
        //nada,lad,hier,pas,ag,cal
        if (t.cm.up) {
            t.px = ((px + 4) / 8) * 8;
            if (0 < py && pasaTanqueM(xx2, yy2 - 1) && pasaTanqueM(xx2 + 1, yy2 - 1) && !tanqueChocaConTanques(t, id, t.dir)) {
                t.py = py - v;
            }
        } else if (t.cm.down) {
            t.px = ((px + 4) / 8) * 8;
            if (py < 192 && pasaTanqueM(xx, yy + 2) && pasaTanqueM(xx + 1, yy + 2) && !tanqueChocaConTanques(t, id, t.dir)) {
                t.py = py + v;
            }
        } else if (t.cm.left) {
            t.py = ((py + 4) / 8) * 8;
            if (0 < px && pasaTanqueM(xx2 - 1, yy2) && pasaTanqueM(xx2 - 1, yy2 + 1) && !tanqueChocaConTanques(t, id, t.dir)) {
                t.px = px - v;
            }//52=51-1
        } else if (t.cm.right) {
            t.py = ((py + 4) / 8) * 8;
            if (px < 192 && pasaTanqueM(xx + 2, yy) && pasaTanqueM(xx + 2, yy + 1) && !tanqueChocaConTanques(t, id, t.dir)) {
                t.px = px + v;
            }//243=51+12*16-1
        } else {
            // no estan presionados los direccionales
            if (matriz[xx + 1][yy + 1] == 5) {//si esta pisando cal (codigo de cal = 5)
                switch (t.dir) {
                    case 0:
                        if ((yy - 1) != (yy - 1) / 4 * 4 && 0 < py && pasaTanqueM(xx2, yy2 - 1) && pasaTanqueM(xx2 + 1, yy2 - 1) && !tanqueChocaConTanques(t, id, t.dir)) {
                            t.py = py - v;
                        }
                        break;
                    case 1:
                        if ((yy - 1) != (yy - 1) / 4 * 4 && py < 192 && pasaTanqueM(xx, yy + 2) && pasaTanqueM(xx + 1, yy + 2) && !tanqueChocaConTanques(t, id, t.dir)) {
                            t.py = py + v;
                        }
                        break;
                    case 2:
                        if ((xx - 1) != (xx - 1) / 4 * 4 && 0 < px && pasaTanqueM(xx2 - 1, yy2) && pasaTanqueM(xx2 - 1, yy2 + 1) && !tanqueChocaConTanques(t, id, t.dir)) {
                            t.px = px - v;
                        }
                        break;
                    case 3:
                        if ((xx - 1) != (xx - 1) / 4 * 4 && px < 192 && pasaTanqueM(xx + 2, yy) && pasaTanqueM(xx + 2, yy + 1) && !tanqueChocaConTanques(t, id, t.dir)) {
                            t.px = px + v;
                        }
                        break;
                }
            }
        }
        //direccion, step
        int d = t.cm.intDir();
        if (d != -1) {
            t.dir = d;
            t.dStep();
        }
    }

    public void gestionItems(Tanque t, byte id) {
        //chequear si el tanque t agarro un item
        boolean itemCapturado = false;
        for (int it = 0; it < enemys.nMaxItem; it++) {
            if (enemys.item[it] != null) {
                boolean iX = (enemys.item[it].x - 16 < t.px && t.px < enemys.item[it].x + 16);
                boolean iY = (enemys.item[it].y - 16 < t.py && t.py < enemys.item[it].y + 16);
                itemCapturado = iX && iY;
                if (itemCapturado) {//item agarrado efecto :: enemys.itemId
                    //setear valores de item
                    enemys.itemTimeEfect[enemys.item[it].id] = enemys.itemTimeEfectMax[enemys.item[it].id];
                    if (id < 2) {
                        players[id].addScore(500);
                    }
                    switch (enemys.item[it].id) {
                        //gestion inicio efecto items
                        case 0:
                            //pala
                            gs.play(gs.ITEM);
                            gDL.efectoItemPala(matriz, enemys.itemTimeEfect[0], id < 2);
                            break;
                        case 1:
                            //escudo
                            enemys.itemTimeEfect[enemys.item[it].id] = 0;
                            gs.play(gs.ITEM);
                            t.efectoItemEscudo(enemys.itemTimeEfectMax[1]);
                            break;
                        case 2:
                            //reloj
                            if (id < 2) {
                                enemys.efectoItemReloj();
                            } else {
                                for (int np = 0; np < nPlayers; np++) {
                                    players[np].tiempoRestanteEfReloj = enemys.itemTimeEfectMax[enemys.item[it].id];
                                }
                                enemys.itemTimeEfect[enemys.item[it].id] = 0;
                            }
                            gs.play(gs.ITEM);
                            break;
                        case 3:
                            //bomba
                            gs.play(gs.ITEM);
                            boolean huboAlMenosUnaExplosion = false;
                            if (id < 2) {
                                for (int ne = 0; ne < enemys.vecEnem.length; ne++) {
                                    if (enemys.vecEnem[ne] != null) {
                                        exp.add(1, enemys.vecEnem[ne].px + 8, enemys.vecEnem[ne].py + 8);
                                        huboAlMenosUnaExplosion = true;
                                        enemys.vecEnem[ne] = null;
                                    }
                                }
                            } else {
                                for (int np = 0; np < nPlayers; np++) {
                                    if (players[np].vivo()) {
                                        matarPlayer(np);
                                        huboAlMenosUnaExplosion = true;
                                    }
                                }
                            }
                            if (huboAlMenosUnaExplosion) {
                                gs.play(gs.EXP_TANK);
                            }
                            break;
                        case 4:
                            //estrella
                            gs.play(gs.ITEM);
                            if (id < 2) {
                                players[id].upGrade();
                            } else {
                                enemys.upGrade(id - 2);
                            }
                            break;
                        case 5:
                            //vida
                            if (id < 2) {
                                players[id].vidas++;
                            } else {
                                enemys.vida((byte)(id - 2));
                            }
                            gs.play(gs.LIFE);
                            break;
                    }
                    enemys.item[it] = null;//eliminar item capturado
                }
            }
        }
    }

    public void matarPlayer(int np) {
        gs.play(gs.EXP_TANK);
        exp.add(1, players[np].px + 8, players[np].py + 8);
        players[np].vidas--;
        players[np].timeRemainingNaciendo = players[np].timeRemainingNaciendoMax;
        players[np].py = 12 * 16;
        if (np < 1) {
            players[np].px = 4 * 16;
        } else {
            players[np].px = 8 * 16;
        }
        players[np].level = 0;//3
        //players[np].upGrade();
        players[np].setValuesLevel(0);
    }

    public void gestionarTanqueDisparo(Tanque t, int id) {
        if (t.cm.buttonA) {
            t.dispararA();
            t.cm.buttonA = false;
        } else if (t.cm.buttonB) {
            t.dispararB();
            t.cm.buttonB = false;
        }
    }

    public void gestionarTanqueBalas(Tanque t, int id) {
        //gestion balas , se analiza una por una (nb)
        for (int nb = 0; nb < t.nBalasEnCurso; nb++) {
            boolean eliminarBala = false;
            int daño = 1;
            if(t.balasEnCurso[nb].tipo==1){
                daño = 3;
            }else if(t.balasEnCurso[nb].tipo==2){
                daño = 5;
            }else{//por si existen otros tipos
                daño = 1;
            }
            int balaVeloc = t.balaVeloc;
            //bala choco con limites del escenario
            if (203 < t.balasEnCurso[nb].px ||
                    t.balasEnCurso[nb].px < 0 ||
                    203 < t.balasEnCurso[nb].py ||
                    t.balasEnCurso[nb].py < 0) {
                eliminarBala = true;
                if(t.balasEnCurso[nb].tipo==0){//bala comun
                    exp.add(0, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_BALA);
                }else if(t.balasEnCurso[nb].tipo==1){//super bala
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }else if(t.balasEnCurso[nb].tipo==2){//laser
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }
            }
            //bala choco con escenario
            int bx = (t.balasEnCurso[nb].px - 2) / 8;//posiciones en la matriz
            int by = (t.balasEnCurso[nb].py - 2) / 8;
            if (!pasaBalaM(bx, by) || !pasaBalaM(bx + 1, by) || !pasaBalaM(bx, by + 1) || !pasaBalaM(bx + 1, by + 1)) {
                eliminarBala = true;
                if(t.balasEnCurso[nb].tipo==0){//bala comun
                    exp.add(0, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_BALA);
                }else if(t.balasEnCurso[nb].tipo==1){//super bala
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }else if(t.balasEnCurso[nb].tipo==2){//laser
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }
                //aqui puedo saber el m[][] donde chocó--->bx, by
                // y tambien la direccon de la bala--->t.balasEnCurso[nb].dir
                //
                //bala choco con ladrillo ---> gestionar daño ladrillo
                if ((t.balasEnCurso[nb].tipo == 0)&&(matriz[bx][by] == 1 || matriz[bx + 1][by] == 1 || matriz[bx][by + 1] == 1 || matriz[bx + 1][by + 1] == 1)) {
                    gs.play(gs.EXP_LADR);
                    int lx1 = bx, ly1 = by;
                    int dir = t.balasEnCurso[nb].dir;
                    switch (dir) {
                        case 1:
                            ly1++;
                            break;
                        case 3:
                            lx1++;
                            break;
                    }
                    int lx2 = lx1, ly2 = ly1;
                    switch (dir) {
                        case 0:
                            lx2++;
                            break;
                        case 1:
                            lx2++;
                            break;
                        case 2:
                            ly2++;
                            break;
                        case 3:
                            ly2++;
                            break;
                    }
                    int pos1 = gDL.posLadrillo(lx1, ly1);
                    int pos2 = gDL.posLadrillo(lx2, ly2);
                    if (pos1 != -1) {
                        gDL.dañarLadrillo(dir, pos1, matriz);
                    }
                    if (pos2 != -1) {
                        gDL.dañarLadrillo(dir, pos2, matriz);
                    }
                }
                //bala choco con piedra ---> gestionar daño piedra
                if ((t.balasEnCurso[nb].tipo == 0)&&(matriz[bx][by] == 6 || matriz[bx + 1][by] == 6 || matriz[bx][by + 1] == 6 || matriz[bx + 1][by + 1] == 6)) {
                    int lx1 = bx, ly1 = by;
                    int dir = t.balasEnCurso[nb].dir;
                    switch (dir) {
                        case 1:
                            ly1++;
                            break;
                        case 3:
                            lx1++;
                            break;
                    }
                    int lx2 = lx1, ly2 = ly1;
                    switch (dir) {
                        case 0:
                            lx2++;
                            break;
                        case 1:
                            lx2++;
                            break;
                        case 2:
                            ly2++;
                            break;
                        case 3:
                            ly2++;
                            break;
                    }
                    int pos1 = gDP.posPiedra(lx1, ly1);
                    int pos2 = gDP.posPiedra(lx2, ly2);
                    boolean destruida1 = false;
                    boolean destruida2 = false;
                    if (pos1 != -1) {
                        destruida1 = gDP.dañarPiedra(pos1, matriz);
                    }
                    if (pos2 != -1) {
                        destruida2 = gDP.dañarPiedra(pos2, matriz);
                    }
                    if (destruida1) {
                        exp.add(2, lx1 * 8 + 3, ly1 * 8 + 4);
                    }
                    if (destruida2) {
                        exp.add(2, lx2 * 8 + 3, ly2 * 8 + 4);
                    }
                    if (destruida1 || destruida2) {
                        gs.play(gs.EXP_PIEDRA);
                    }
                }
                //super bala : rompe hierro = 1
                if (t.balasEnCurso[nb].tipo == 1 || t.balasEnCurso[nb].tipo == 2) {//tipo = {1 o 2}
                    boolean m00 = (matriz[bx][by] == 1 || (matriz[bx][by] == 2) || (matriz[bx][by] == 6));
                    boolean m01 = (matriz[bx][by + 1] == 1 || (matriz[bx][by + 1] == 2) || (matriz[bx][by + 1] == 6));
                    boolean m10 = (matriz[bx + 1][by] == 1 || (matriz[bx + 1][by] == 2) || (matriz[bx + 1][by] == 6));
                    boolean m11 = (matriz[bx + 1][by + 1] == 1 || (matriz[bx + 1][by + 1] == 2) || (matriz[bx + 1][by + 1] == 6));
                    if (m00) {
                        matriz[bx][by] = 0;// por las dudas si es hierro
                        gDL.eliminarLadrillo(matriz, gDL.posLadrillo(bx, by));
                        gDP.eliminarPiedra(matriz, gDP.posPiedra(bx, by));
                    }
                    if (m01) {
                        matriz[bx][by + 1] = 0;// por las dudas si es hierro
                        gDL.eliminarLadrillo(matriz, gDL.posLadrillo(bx, by + 1));
                        gDP.eliminarPiedra(matriz, gDP.posPiedra(bx, by + 1));
                    }
                    if (m10) {
                        matriz[bx + 1][by] = 0;// por las dudas si es hierro
                        gDL.eliminarLadrillo(matriz, gDL.posLadrillo(bx + 1, by));
                        gDP.eliminarPiedra(matriz, gDP.posPiedra(bx + 1, by));
                    }
                    if (m11) {
                        matriz[bx + 1][by + 1] = 0;// por las dudas si es hierro
                        gDL.eliminarLadrillo(matriz, gDL.posLadrillo(bx + 1, by + 1));
                        gDP.eliminarPiedra(matriz, gDP.posPiedra(bx + 1, by + 1));
                    }
                }
            }
            //bala choca con tanques
            if (balaChocaConTanques(nb, t, id, t.balasEnCurso[nb].dir, daño)) {
                eliminarBala = true;
                if(t.balasEnCurso[nb].tipo==0){//bala comun
                    exp.add(0, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_BALA);
                }else if(t.balasEnCurso[nb].tipo==1){//super bala
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }else if(t.balasEnCurso[nb].tipo==2){//laser
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }
            }
            //bala choco con otra bala
            if (balaChocaConBalas(nb, t, id, t.balasEnCurso[nb].dir)) {
                eliminarBala = true;
                if(t.balasEnCurso[nb].tipo==0){//bala comun
                    exp.add(0, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_BALA);
                }else if(t.balasEnCurso[nb].tipo==1){//super bala
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }else if(t.balasEnCurso[nb].tipo==2){//laser
                    exp.add(3, t.balasEnCurso[nb].px, t.balasEnCurso[nb].py);
                    gs.play(gs.EXP_SUPERBALA);
                }
            }
            //cambiar posic de balas
            int dx = 0, dy = 0;
            switch (t.balasEnCurso[nb].dir) {
                case 0:
                    dy = -balaVeloc;
                    break;
                case 1:
                    dy = +balaVeloc;
                    break;
                case 2:
                    dx = -balaVeloc;
                    break;
                case 3:
                    dx = +balaVeloc;
                    break;
                default:/*nothing*/
                    break;
            }
            t.balasEnCurso[nb].px += dx;
            t.balasEnCurso[nb].py += dy;
            if(eliminarBala){
                t.eliminarBala(nb);
            }
        }
    }

    public void gestionarTanque(Tanque t, byte id) {
        // int id: players (0,1), enemys (2,3,4,...)
        // se utiliza para identificar el tanque gestionado para
        // interactuar con los demas menos consigo mismo.
        if (t.timeRemainingNaciendo < 1 && ((1 < id) || aguilaEstado == 0)) {
            //si es player el tanque t 
            if (1 < id || players[id].tiempoRestanteEfReloj < 1) {
                //si es enemigo o si es un player sin efecto reloj
                gestionarTanqueMov(t, id);
            } else {
                players[id].tiempoRestanteEfReloj--;
            }
            gestionarTanqueDisparo(t, id);
        } else {
            t.timeRemainingNaciendo--;
            if (t.timeRemainingNaciendo < 1 && id < 2 && aguilaEstado == 0) {
                if (id < 2) {
                    //escudo de nacimiento solo para player
                    t.efectoItemEscudo(enemys.itemTimeEfectMax[1]);
                    t.escudoTime = t.escudoTime / 4;//escudo
                }

            }
        }
        gestionarTanqueBalas(t, id);
        t.gestionarEscudo();
    }

    private void demostrando() throws Exception {
        System.out.println("demostrando");
        pantalla = Pantalla.stageJugando;
        stageJugando();
        Thread.sleep(1000);
    }

    private void puntuando() throws Exception {
        System.out.println("puntuando");
        pantalla = Pantalla.puntuando;
        tT = 0;
        countTTP1 = new int[4];//todos ceros (0,0,0,0)
        countTTP2 = new int[4];//todos ceros (0,0,0,0)
        newScore[0] = 0;
        newScore[1] = 0;
        int[] vecCountP1 = players[0].tTanquesVencidos;
        int[] vecCountP2 = players[1].tTanquesVencidos;
        //por cada tipo de tanque vencido "tT"
        for (int tt = 0; tt < 4; tt++) {//0,1,2,3 : tipos de tanque enemigo
            tT = tt;
            //por cada tanque vencido "c" del tipo tT
            dibuja();
            gs.play(gs.STEP);
            Thread.sleep(300);
            int c = 0;
            while (c < vecCountP1[tT] || c < vecCountP2[tT]) {
                if (c < vecCountP1[tT]) {
                    countTTP1[tT]++;
                    newScore[0] = newScore[0] + enemys.getScoreTankType(tT);
                }
                if (c < vecCountP2[tT]) {
                    countTTP2[tT]++;
                    newScore[1] = newScore[1] + enemys.getScoreTankType(tT);
                }
                dibuja();
                gs.play(gs.COIN);
                Thread.sleep(60);
                c++;
            }
            Thread.sleep(300);
        }
        players[0].addScore(newScore[0]);
        players[1].addScore(newScore[1]);
        players[0].vaciarTanquesVencidos();
        players[1].vaciarTanquesVencidos();
        Thread.sleep(500);
        stage++;
        if (stageFinal < stage) {
            stage = 0;
        }
    }

    private void recordMostrando() throws Exception {
        System.out.println("recordMostrando");
        pantalla = Pantalla.recordMostrando;
        //determinar si se supero el hiscore
        boolean hiScoreSuperado = false;
        for (int np = 0; np < nPlayers; np++) {
            if (hiScore < players[np].score) {
                hiScoreSuperado = true;
                hiScore = players[np].score;
            }
        }
        //mostrar "festejo" si se supero el hiscore
        if (hiScoreSuperado) {
            tpo = 255;
            while (0 < tpo) {
                dibuja();
                Thread.sleep(10);
                tpo--;
            }
            Thread.sleep(100);
        }
    }

    private void stageGanando() throws Exception {
        System.out.println("stageGanando");
        pantalla = Pantalla.stageGanando;
        tpo = 255;
        gs.play(gs.WIN);
        dibuja();
        Thread.sleep(500);
        while (0 < tpo) {
            dibuja();
            Thread.sleep(10);
            tpo--;
        }
        Thread.sleep(1000);
    }

    private void stagePerdiendo() throws Exception {
        System.out.println("stagePerdiendo");
        pantalla = Pantalla.stagePerdiendo;
        tpo = 255;
        gs.play(gs.LOSE);
        dibuja();
        Thread.sleep(500);
        while (0 < tpo) {
            dibuja();
            Thread.sleep(10);
            tpo--;
        }
        Thread.sleep(1000);
    }

    private void gameOver() throws Exception {
        System.out.println("gameOver");
        pantalla = Pantalla.gameOver;
        tpo = 255;
        gs.play(gs.GAME_OVER);
        dibuja();
        Thread.sleep(500);
        while (0 < tpo) {
            dibuja();
            Thread.sleep(10);
            tpo--;
        }
        Thread.sleep(1000);
    }

    public void net() throws Exception {
        System.out.println("net");
        pantalla = Pantalla.net;
        mensajeNet = "waiting Server";
        dibuja();
        Thread.sleep(1000);
        // 
        if(clientInstance == null){
            clientInstance = new ClientInstance(host, PORT);
        }
        if(!clientInstance.isConnected()){
            mensajeNet = "waiting Server : conecting...";
            dibuja();
            Thread.sleep(1000);
            clientInstance.init();
        }
        mensajeNet = "Conection successful!";
        dibuja();
        Thread.sleep(1000);
        int counter = 0;
        while(clientInstance.wait){
            mensajeNet = "Waiting other player ... ("+counter+" sec)";
            dibuja();
            Thread.sleep(1000);
            counter++;
        }
        idPlayer = clientInstance.idPlayer;
        mensajeNet = "Player "+idPlayer+" GO !";
        dibuja();
        Thread.sleep(2000);
        pantalla = Pantalla.stageSeleccionando;
    }
    
    public void opciones() throws Exception {
        System.out.println("opciones");
        pantalla = Pantalla.opciones;
        players[0].cm.falseAll_menosStart();
        opc = 0;
        t = 0;
        boolean toggle = true;
        int timeDead = 0;//tiempo transcurrido (en unidades del sleep)
        //sinque se presione up, down, start
        //gs.inicializar();
        boolean soltoStart = false;
        do {
            dibuja();
            Thread.sleep(20);
            timeDead++;
            t++;
            if (toggle) {
                //up down presionados
                if (players[0].cm.up) {
                    gs.play(gs.COIN);
                    if (opc < 1) {
                        opc = 1;
                        //opc = 2;
                    } else {
                        opc--;
                    }
                    toggle = false;
                } else if (players[0].cm.down) {
                    gs.play(gs.COIN);
                    if (opc > 0) {
                    //if (opc > 1) {
                        opc = 0;
                    } else {
                        opc++;
                    }
                    toggle = false;
                } else if (players[0].cm.left) {
                    if(opc == 0){//difficulty -
                        if(0 < indexDifficulty){
                            gs.play(gs.COIN);
                            indexDifficulty--;
                        }
                    }else{//speed -
                        if(0 < indexSpeed){
                            gs.play(gs.COIN);
                            indexSpeed--;
                        }
                    }
                    toggle = false;
                } else if (players[0].cm.right) {
                    if(opc == 0){// difficulty +
                        if(indexDifficulty < difficulty.length-1){
                            gs.play(gs.COIN);
                            indexDifficulty++;
                        }
                    }else{// speed +
                        if(indexSpeed < speed.length-1){
                            gs.play(gs.COIN);
                            indexSpeed++;
                        }
                    }
                    toggle = false;
                }
            } else {
                if (!players[0].cm.up && !players[0].cm.down && !players[0].cm.left && !players[0].cm.right) {
                    //up down liberados (se sualtan los botones direccionales en cuestion)
                    toggle = true;
                    timeDead = 0;
                }
            }
            if (!players[0].cm.start) {
                soltoStart = true;
            }
        } while (timeDead < 500 && !players[0].cm.start || !soltoStart);
        
        gs.play(gs.SELECTION);
        Thread.sleep(500);
        
    }

    ////////////////////////////////////////////////////////////////////////////
    public void mostrarControles(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.drawImage(imgKeyboard, 0, 0, ANCHO*kx/4, ALTO*ky/3, this);
        g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 40*ky/3));
        if((t/30)%2 == 0){
            dString(g, "PRESS START", (1000)*kx/4, (1000)*ky/3);
        }
    }
    
    public void mostrarPresentando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.drawImage(imgPresentando, 200*kx/4, 800*ky/3 - 3 * t*ky/3, ANCHO*kx/4 - 400*kx/4, ALTO*ky/3 - 400*ky/3, this);
    }

    public void mostrarOpcionSeleccionando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.drawImage(imgPresentando, 200*kx/4, 50*ky/3, ANCHO*kx/4 - 400*kx/4, ALTO*ky/3 - 400*ky/3, this);
        g.setColor(Color.white);
        Font font = new Font("RECStyle", 0, 40*ky/3);
        g.setFont(font);
        dString(g,"Hi Score :  " + hiScore, 460, 40);
        dString(g,"1 Player", 600, 450);
        dString(g,"2 Players", 600, 500);
        dString(g,"Construction", 600, 550);
        //dString(g,"2 Players Net", 600, 650);
        dString(g,"Options", 600, 600);
        g.drawImage(imgCursor, 500*kx/4, (410 + 50 * opc)*ky/3, 64*kx/4, 48*ky/3, this);
    }

    public void mostrarStageSeleccionando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.setColor(Color.gray);
        Font font = new Font("RECStyle", 0, 25*ky/3);
        g.setFont(font);
        
        if(useCustomMaps){
            dString(g,"Custom Maps", 550, 170);
        }else{
            dString(g,"Default maps", 550, 170);
        }
        
        dString(g,"STAGE  " + stage, 550, 200);
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (0 < m) {
                    dImage(g,imgEscenario, 400+16*(i), 250+12*(j), 400+16*(i+1), 250+12*(j+1),
                           imgX0(m), imgY0(m), imgX0(m)+8, imgY0(m)+8, this);
                }
            }
        }
        
        Font font2 = new Font("RECStyle", 0, 20*ky/3);
        g.setFont(font2);
        dString(g,"use buttonA / buttonB key to change the stage", 200, 600);
        dString(g,"use arrows to change default/custom maps", 200, 620);
        dString(g,"use start key to start the game", 200, 645);
    }

    public void mostrarStagePresentando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        Font font = new Font("RECStyle", 0, 25*ky/3);
        g.setFont(font);
        Color c = new Color(t, t, t);
        g.setColor(c);
        dString(g,"STAGE  " + stage, 550, 300);
    }

    public void mostrarConstruyendo(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.setColor(Color.black);
        g.fillRect(200*kx/4, 50*ky/3, 832*kx/4, 624*ky/3);
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (0 < m) {
                    dImage(g,imgEscenario, i * 32 + 200, j * 24 + 50, i * 32 + 32 + 200, j * 24 + 24 + 50,
                            imgX0(m), imgY0(m), imgX0(m) + 8, imgY0(m) + 8, this);
                }
            }
        }
        //aguila y cursor
        dImage(g,imgEscenario, aguilaX * 32 + 200, aguilaY * 24 + 50, aguilaX * 32 + 64 + 200, aguilaY * 24 + 48 + 50, 32 + 16 * aguilaEstado, 16, 48 + 16 * aguilaEstado, 32, this);
        if (flag) {
            g.drawImage(imgCursor, (x * 32 + 200)*kx/4, (y * 24 + 50)*ky/3, 32*kx/4, 24*ky/3, this);
        }
        //info
        g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 15));
        g.setColor(Color.WHITE);
        dString(g, "Use arrow to move the cursor", 20, 20);
        dString(g, "Use A/B to change tile", 20, 40);
        dString(g, "Use start to confirm or to exit", 20, 60);
    }

    public void mostrarSaveStage(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.setColor(Color.gray);
        g.fillRect(200*kx/4, 200*ky/3, ANCHO*kx/4 - 400*kx/4, ALTO*ky/3 - 400*ky/3);
        g.setColor(Color.black);
        g.fillRect(648*kx/4, 250*ky/3, 20*20*kx/4, 5*40*ky/3);
        g.setFont(new Font(Font.MONOSPACED, Font.BOLD, 25*ky/3));
        dString(g,"---------------SAVE STAGE---------------", 250, 240);
        if(loadNumberValid){
            g.setColor(Color.LIGHT_GRAY);
        }else{
            g.setColor(Color.RED.darker());
        }
        dString(g,">", 200, loadOpc * 40 + 280);
        dString(g,"STAGE NUMBER: " + this.loadStage, 250, 280);
        g.setColor(Color.LIGHT_GRAY);
        int sco = 0;
        int columnas = 20;
        int px = 0;
        int py = 0;
        String sTTank = "";
        // calculo de longitud del vector-matriz
        int lenghtM = 0;
        int max = enemys.maxEnemysForLevel;
        boolean sent = false;
        while(lenghtM<max && !sent){
            if(loadTiposT[lenghtM]==-1){
                sent = true;
            }
            lenghtM++;
        }
        for(int i=lenghtM;i<max;i++){
            loadTiposT[i]=-1;
        }
        for (int i = 0; i < lenghtM; i++) {
            if(-1<loadTiposT[i]){
                sTTank = "" + loadTiposT[i];
                drawMinitank(g, loadTiposT[i], px, py);
            }else{
                sTTank = ".";
            }
            dString(g,sTTank, 650 + px * 20, 280 + py * 40);
            sco = sco + (loadTiposT[i] + 1) * 100;
            //calculo de px y py 
            px++;
            if (columnas - 1 < px) {
                px = px - columnas;
                py++;
            }
        }
        int tpy = 0;
        int tpx = loadIndex;
        while (columnas - 1 < tpx) {
            tpy++;
            tpx = tpx - columnas;
        }
        dString(g,"_", 650 + tpx * 20, 280 + tpy * 40);
        dString(g,"TANKS:(matrix)"+sco, 250, 320);
        dString(g,"TIME DEAD: " + this.loadTimeEntreT, 250, 360);
        dString(g,"MAX CONCURRENCY " + this.loadEnemySimul, 250, 400);
        dString(g,"SAVE", 250, 440);
        Font font2 = new Font(Font.MONOSPACED, Font.BOLD, 18*ky/3);
        g.setFont(font2);
        dString(g,"(up, down):select - (A, B):change values - (start):EXIT", 300, 475);
        if (loadOpc == 4) {
            dString(g,"AND SAVE", 300, 495);
        }
        
        // info
        String helpString  = "";
        String helpString2 = "";
        String helpString3 = "";
        switch(loadOpc){
            case 0:
                helpString = "select the stage number to save the map (it is not overwritten)";
                break;
            case 1:
                helpString = "The numbers represent the type of tank (0: the weakest, 3 the strongest)";
                helpString2= "left-right : move, A : increase, B: decrease";
                helpString3= "note : the dot represent the end of the list";
                break;
            case 2:
                helpString = "dead time between birth of each enemy tank";
                break;
            case 3:
                helpString = "number of tanks can be simultaneously on the map";
                break;
            case 4:
                helpString = "save the map";
                break;
        }
        dString(g, "-INFO-", 150, 600);
        dString(g, helpString, 150, 620);
        dString(g, helpString2, 150, 640);
        dString(g, helpString3, 150, 660);
    }

    public void mostrarStageJugando(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.setColor(Color.black);
        int ancho = 832, alto = 624;
        g.fillRect(200*kx/4, 51*ky/3, ancho*kx/4, alto*ky/3);
        //info pantalla
        Font font = new Font("RECStyle", 0, 20*ky/3);
        g.setFont(font);
        int jTipo, j0, py, px;
        j0 = enemys.nVTTE;
        int jf = enemys.maxEnemysForLevel;
        for (int j = j0; j < jf; j++) {
            jTipo = enemys.vectorTipoTanquesEnemy[j];
            py = (j / 8) * 24;
            px = (j - j / 8 * 8) * 32;
            Image img;
            if (jTipo < 3) {
                img = imgEnemyNoBlind[1];
            } else {
                img = imgEnemyBlind[1];
                jTipo = 0;
            }
            dImage(g,img, px + 1040, py + 100, px + 1072, py + 124, 0, jTipo * 16, 16, jTipo * 16 + 16, this);
        }
        int max = enemys.maxEnemysForLevel;
        int qe = 0;
        boolean detener = false;
        while(qe<max && !detener){
            if(enemys.vectorTipoTanquesEnemy[qe]<0){
                detener = true;
            }else{
                qe++;
            }
        }
        String str = "Enemies  ";
        if(0<(qe-enemys.nVTTE)){str=str+(qe-enemys.nVTTE);}else{str=str+"0";}
        dString(g,str, 1070, 80);
        dString(g,"Player 1    " + players[0].score, 1070, 460);
        if (players[0].vivo()) {
            dString(g,"Lives :" + players[0].vidas, 1070, 490);
        } else {
            dString(g,"Game Over", 1070, 490);
        }
        dString(g,"", 1070, 490);
        dString(g,"Player 2    " + players[1].score, 1070, 550);
        if (players[1].vivo()) {
            dString(g,"Lives :" + players[1].vidas, 1070, 580);
        } else {
            dString(g,"Game Over", 1070, 580);
        }
        dString(g,"", 1070, 610);
        dString(g,"Stage    # " + stage, 1070, 640);
        dString(g,"Max:" + enemys.maxEnemysSimul+"  t:"+enemys.tpoEntreNacimiento, 1070, 670);
        
        dString(g,"Difficulty level: "+difficulty[indexDifficulty] , 300, 40);
        dString(g,"Game speed: "+speed[indexSpeed] , 600, 40);
        
      //info blindaje
        int k0 = 2*players[0].blindaje-2;
        int k1 = 2*players[1].blindaje-2;
        g.setColor(Color.GREEN);
        g.fillRect(1200*kx/4, 475*ky/3, k0*kx/4, 15*ky/3);
        g.fillRect(1200*kx/4, 565*ky/3, k1*kx/4, 15*ky/3);
        //  cal
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (5 == m) {
                    dImage(g,imgEscenario, i * 32 + 200, j * 24 + 51, i * 32 + 32 + 200, j * 24 + 24 + 51,
                            112, 0, 120, 8, this);
                }
            }
        }
        if (30 < t) {
            t = 0;
            x = 1 - x;
        }
        // agua
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (4 == m) {
                    dImage(g,imgEscenario, i * 32 + 200, j * 24 + 51, i * 32 + 32 + 200, j * 24 + 24 + 51,
                            104, 0 + 8 * x, 112, 8 + 8 * x, this);
                }
            }
        }
        t++;
        // balas Player
        // nada,lad,hier,pas,ag,cal
        for (int np = 0; np < 2; np++) {
            for (int nb = 0; nb < players[np].nBalasEnCurso; nb++) {
                int bx = players[np].balasEnCurso[nb].px;
                int by = players[np].balasEnCurso[nb].py;
                int dy = players[np].balasEnCurso[nb].dir * 8;
                int rx = 88;
                int ry = 96;
                if (players[np].balasEnCurso[nb].tipo == 1) {//super balas
                    rx = 96;
                }else if(players[np].balasEnCurso[nb].tipo == 2){//laser
                    rx = 80;
                }
                dImage(g,imgEscenario, bx*4+200, by*3+51, bx*4+16+200, by*3+12+51, rx, 8+dy, rx+8, 16+dy, this);
            }
        }

        //players
        for (int np = 0; np < 2; np++) {
            if (players[np].vivo()) {
                int trn = players[np].timeRemainingNaciendo;
                if (trn < 1) {
                    //tanque ya nacido
                    int x0 = players[np].dir * 16 + 4 * 16 * players[np].step;
                    int y0 = players[np].level * 16;
                    dImage(g,imgPlayer[np],
                            players[np].px * 4 + 200, players[np].py * 3 + 51, players[np].px * 4 + 64 + 200, players[np].py * 3 + 48 + 51,
                            x0, y0, x0 + 16, y0 + 16, this);
                } else {
                    //tanque naciendo
                    int ind = players[np].indexTRN();
                    int nx = players[np].px * 4 + 200;
                    int ny = players[np].py * 3 + 51;
                    dImage(g,imgEscenario, nx, ny, nx + 16 * 4, ny + 16 * 3, ind * 16, 32, ind * 16 + 16, 48, this);
                }
                //escudo item (o escudo de nacimiento)
                if (players[np].escudoActivado) {
                    dImage(g,imgEscenario,
                            players[np].px * 4 + 200, players[np].py * 3 + 51, players[np].px * 4 + 64 + 200, players[np].py * 3 + 48 + 51,
                            0 + 16 * players[np].escudoEstado, 16, 16 + 16 * players[np].escudoEstado, 32, this);
                    players[np].dEscudoEstado();
                }
            }
        }
        //aguila
        dImage(g,imgEscenario, aguilaX * 32 + 200, aguilaY * 24 + 50, aguilaX * 32 + 64 + 200, aguilaY * 24 + 48 + 50, 32 + 16 * aguilaEstado, 16, 48 + 16 * aguilaEstado, 32, this);
        // balas Enemy
        for (int ne = 0; ne < enemys.vecEnem.length; ne++) {
            if (enemys.vecEnem[ne] != null) {
                for (int nb = 0; nb < enemys.vecEnem[ne].nBalasEnCurso; nb++) {
                    int bx = enemys.vecEnem[ne].balasEnCurso[nb].px;
                    int by = enemys.vecEnem[ne].balasEnCurso[nb].py;
                    int dy = enemys.vecEnem[ne].balasEnCurso[nb].dir * 8;
                    int rx = 88;
                    int ry = 96;
                    if (enemys.vecEnem[ne].balasEnCurso[nb].tipo == 1) {//super balas
                        rx = 96;
                        ry = 104;
                    }
                    dImage(g,imgEscenario, bx * 4 + 200, by * 3 + 51, bx * 4 + 16 + 200, by * 3 + 12 + 51, rx, 8 + dy, ry, 16 + dy, this);
                }
            }
        }//enemys
        for (int ne = 0; ne < enemys.vecEnem.length; ne++) {
            if (enemys.vecEnem[ne] != null) {
                int trn = enemys.vecEnem[ne].timeRemainingNaciendo;
                if (trn < 1) {
                    int xx0 = enemys.vecEnem[ne].dir * 16 + 4 * 16 * enemys.vecEnem[ne].step;
                    int tipo = enemys.vecEnem[ne].tipo;
                    int color = enemys.vecEnem[ne].blindaje;
                    if (4 < color) {
                        color = 0;
                    }
                    if (enemys.vecEnem[ne].itemPortado) {
                        if (enemys.vecEnem[ne].lightItem) {
                            color = 0;
                        }
                        enemys.vecEnem[ne].dLigthItem();
                    }
                    Image img;
                    if (2 < tipo) {
                        tipo = 0;
                        img = imgEnemyBlind[color];
                    } else {
                        img = imgEnemyNoBlind[color];
                    }
                    dImage(g,img,
                            enemys.vecEnem[ne].px * 4 + 200,
                            enemys.vecEnem[ne].py * 3 + 51,
                            enemys.vecEnem[ne].px * 4 + 64 + 200,
                            enemys.vecEnem[ne].py * 3 + 48 + 51,
                            xx0, tipo * 16, xx0 + 16, tipo * 16 + 16, this);
                } else {
                    int ind = enemys.vecEnem[ne].indexTRN();
                    int nx = enemys.vecEnem[ne].px * 4 + 200;
                    int ny = enemys.vecEnem[ne].py * 3 + 51;
                    dImage(g,imgEscenario, nx, ny, nx + 16 * 4, ny + 16 * 3, ind * 16, 32, ind * 16 + 16, 48, this);
                }
                if (enemys.vecEnem[ne].escudoActivado) {
                    //escudo item
                    dImage(g,imgEscenario,
                            enemys.vecEnem[ne].px * 4 + 200, enemys.vecEnem[ne].py * 3 + 51, enemys.vecEnem[ne].px * 4 + 64 + 200, enemys.vecEnem[ne].py * 3 + 48 + 51,
                            0 + 16 * enemys.vecEnem[ne].escudoEstado, 16, 16 + 16 * enemys.vecEnem[ne].escudoEstado, 32, this);
                    enemys.vecEnem[ne].dEscudoEstado();
                }
            }
        }
        // hierro , pasto
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (1 < m && m < 4) {
                    dImage(g,imgEscenario, i * 32 + 200, j * 24 + 51, i * 32 + 32 + 200, j * 24 + 24 + 51,
                            imgX0(m), imgY0(m), imgX0(m) + 8, imgY0(m) + 8, this);
                }
            }
        }
        // ladrillo
        int posLadrillo = 0;
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (1 == m) {
                    posLadrillo = gDL.posLadrillo(i, j);
                    if (posLadrillo == -1) {
                        System.out.println("ERROR.game2.MostrarStageJugando.ladrillos");
                    }
                    if (gDL.vectorLadrillos[posLadrillo].c[3]) {
                        dImage(g,imgEscenario, i * 32 + 200, j * 24 + 51, i * 32 + 16 + 200, j * 24 + 12 + 51, 80, 0, 84, 4, this);
                    }
                    if (gDL.vectorLadrillos[posLadrillo].c[2]) {
                        dImage(g,imgEscenario, i * 32 + 200 + 16, j * 24 + 51, i * 32 + 32 + 200, j * 24 + 12 + 51, 84, 0, 88, 4, this);
                    }
                    if (gDL.vectorLadrillos[posLadrillo].c[1]) {
                        dImage(g,imgEscenario, i * 32 + 200, j * 24 + 51 + 12, i * 32 + 16 + 200, j * 24 + 24 + 51, 80, 4, 84, 8, this);
                    }
                    if (gDL.vectorLadrillos[posLadrillo].c[0]) {
                        dImage(g,imgEscenario, i * 32 + 200 + 16, j * 24 + 51 + 12, i * 32 + 32 + 200, j * 24 + 24 + 51, 84, 4, 88, 8, this);
                    }
                }
            }
        }
        // piedra
        int posPiedra = 0;
        for (int i = 0; i < matrizDimension; i++) {
            for (int j = 0; j < matrizDimension; j++) {
                int m = matriz[i][j];
                if (6 == m) {
                    posPiedra = gDP.posPiedra(i, j);
                    int vida = gDP.vectorPiedra[posPiedra].piedraVida;
                    int dy = (4 - ((vida + 1) / 2)) * 8;
                    dImage(g,imgEscenario, i * 32 + 200, j * 24 + 51, i * 32 + 32 + 200, j * 24 + 24 + 51,
                            imgX0(m), imgY0(m) + dy, imgX0(m) + 8, imgY0(m) + dy + 8, this);
                }
            }
        }
        //explosiones
        int[] z = new int[8];
        for (int e = 0; e < exp.nMax; e++) {
            if (exp.ex[e] != null) {
                z = exp.getValues(e);
                dImage(g,imgExplosiones, z[0] * 4 + 200, z[1] * 3 + 51, z[2] * 4 + 200, z[3] * 3 + 51, z[4], z[5], z[6], z[7], this);
            }
        }
        //items
        for (int i = 0; i < enemys.nMaxItem; i++) {
            if (enemys.item[i] != null) {
                if (enemys.item[i].id < 5) {//item = 0,1,2,3,4
                    dImage(g,imgEscenario,
                            enemys.item[i].x * 4 + 200, enemys.item[i].y * 3 + 51, enemys.item[i].x * 4 + 16 * 4 + 200, enemys.item[i].y * 3 + 16 * 3 + 51,
                            16 * enemys.item[i].id, 0, 16 * enemys.item[i].id + 16, 16, this);
                } else {              //item = 5
                    dImage(g,imgEscenario,
                            enemys.item[i].x * 4 + 200, enemys.item[i].y * 3 + 51, enemys.item[i].x * 4 + 16 * 4 + 200, enemys.item[i].y * 3 + 16 * 3 + 51,
                            64, 16, 64 + 16, 16 + 16, this);
                }
            }
        }
        //begin stage
        if (begin) {
            g.setColor(new Color(0f, 0f, 0f, 2 - tBegin));
            g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        //g.fillRect(200, 51, 832, 624);
        }
        // info demostrando
        if (demostrando) {
            g.setColor(Color.white);
            int tamanoLetraDem = 40;
            Font fontDem = new Font(Font.DIALOG, 0, tamanoLetraDem*ky/3);
            g.setFont(fontDem);
            dString(g,"DEMONSTRATION", (ANCHO - 9 * tamanoLetraDem) / 2, (ALTO - tamanoLetraDem) / 2);
        }
        //pausa
        Font fontPause = new Font("RECStyle", 0, 40*ky/3);
        if (pause) {
            g.setColor(new Color(0f, 0f, 0f, 0.7f));
            g.fillRect(200*kx/4, 51*ky/3, 832*kx/4, 624*ky/3);
            tPause++;
            if (10 < tPause) {
                tPause = 0;
                mostrarPause = !mostrarPause;
            }
            if (mostrarPause) {
                g.setColor(Color.red);
                g.setFont(fontPause);
                dString(g,"Pause", ANCHO / 2 - 60, ALTO / 2 - 20);
            }
        }
    }

    public int imgX0(int img) {
        if (0 < img) {
            return (72 + img * 8);
        } else {
            return 80;
        }
    }

    public int imgY0(int img) {
        if (0 < img) {
            return 0;
        } else {
            return 8;
        }
    }

    public void mostrarStageGanando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        Font font = new Font("RECStyle", 0, 100*ky/3);
        g.setFont(font);
        Color c = new Color(0, tpo, 0);
        g.setColor(c);
        dString(g,"YOU WIN !", 300, 400);
    }

    public void mostrarStagePerdiendo(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        Font font = new Font("RECStyle", 0, 100*ky/3);
        g.setFont(font);
        Color c = new Color(tpo, 0, 0);
        g.setColor(c);
        dString(g,"YOU LOSE !", 300, 400);
    }

    public void mostrarPuntuando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        // mostrar titulos y numeros (tT)
        Font fontTitle = new Font(Font.MONOSPACED, Font.BOLD, 50*ky/3);
        g.setFont(fontTitle);
        g.setColor(Color.WHITE);
        dString(g,"Stage #" + stage, 520, 120);
        Font fontBlock = new Font(Font.MONOSPACED, Font.BOLD, 26*ky/3);
        g.setFont(fontBlock);
        dString(g,"Player 1", 250, 200);
        dString(g,"Player 2", 900, 200);
        for (int jTipo = 0; jTipo < tT + 1; jTipo++) {
            dString(g,enemys.getScoreTankType(jTipo) + " x " + countTTP1[jTipo], 400, 80 * jTipo + 270);
            dString(g,enemys.getScoreTankType(jTipo) + " x " + countTTP2[jTipo], 1000, 80 * jTipo + 270);
        }
        g.drawLine(290*kx/4, 550*ky/3, 1200*kx/4, 550*ky/3);
        dString(g,"" + newScore[0], 400, 615);
        dString(g,"" + newScore[1], 1000, 615);
        // mostrar tanques (tT)
        for (int j = 0; j <= tT; j++) {//4 tipos de tanques
            int jTipo = j;
            Image img;
            if (jTipo < 3) {
                img = imgEnemyNoBlind[1];
            } else {
                img = imgEnemyBlind[1];
                jTipo = 0;
            }
            dImage(g,img, 720, j * 80 + 215, 720 + 90, j * 80 + 215 + 71, 0, jTipo * 16, 16, jTipo * 16 + 16, null);
        }

    }
    public void dImage(Graphics g,Image img,int x0,int y0,int x,int y,int b0,int a0,int b,int a, Object o){
        g.drawImage(img, x0*kx/4, y0*ky/3, x*kx/4, y*ky/3, b0, a0, b, a, null);
    }
    public void dString(Graphics g, String str, int x0, int y0){
        g.drawString(str, x0*kx/4, y0*ky/3);
    }

    public void mostrarDemostrando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
    }

    public void mostrarRecordMostrando(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        Font font = new Font("RECStyle", 0, 100*ky/3);
        g.setFont(font);
        Color c = new Color(0, tpo, tpo / 2);
        g.setColor(c);
        g.drawString("HI SCORE " + hiScore, 200*kx/4, 400*ky/3);
    }

    public void mostrarGameOver(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        Font font = new Font("RECStyle", 0, 100*ky/3);
        g.setFont(font);
        Color c = new Color(tpo, 0, tpo / 2);
        g.setColor(c);
        g.drawString("GAME OVER", 300*kx/4, 400*ky/3);
    }
    
    public void mostrarNet(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        Font font = new Font(Font.MONOSPACED, Font.BOLD, 40*ky/3);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString("2 Players on Net", 200*kx/4, 200*ky/3);
        Font font2 = new Font(Font.MONOSPACED, Font.BOLD, 25*ky/3);
        g.setFont(font2);
        g.drawString(mensajeNet, 300*kx/4, 250*ky/3);
        g.drawString("Battle City 1.0 OnLine", 300*kx/4, 290*ky/3);
    }
    
    public void mostrarOpciones(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(0, 0, ANCHO*kx/4, ALTO*ky/3);
        g.drawImage(imgPresentando, 300*kx/4, 100*ky/3, ANCHO*kx/4 - 600*kx/4, ALTO*ky/3 - 600*ky/3, this);
        g.setColor(Color.white);
        g.setFont(new Font("RECStyle", 0, 40*ky/3));
        dString(g,"OPTIONS", 300, 380);
        g.setFont(new Font("RECStyle", 0, 25*ky/3));
        dString(g,"Difficulty level", 400, 450);
        dString(g,"Game speed", 400, 500);
        
        g.setColor(Color.GRAY);
        g.setFont(new Font("RECStyle", Font.BOLD, 30*ky/3));
        dString(g,difficulty[indexDifficulty], 700, 450);
        dString(g,speed[indexSpeed], 700, 500);
        
        g.setColor(Color.WHITE);
        g.setFont(new Font("RECStyle", 0, 20*ky/3));
        //dString(g,"...", 400, 550);
        dString(g,"use left/right to change values, start to exit menu", 300, 600);
        //dString(g,"2 Players Net", 600, 600);
        g.drawImage(imgCursor, 300*kx/4, (418 + 50 * opc)*ky/3, 64*kx/4, 48*ky/3, this);
        
        int opacidad = (int) (100.0+30.0 * Math.sin(0.15*t));
        g.setColor(new Color(255, 255, 255, opacidad));
        g.fillRect(0, (418 + 50 * opc)*ky/3, 2000*kx/4, 48*ky/3);
    }
    ////////////////////////////////////////////////

    private void inicializarGame() {//OBLIGATORIO
        //inicializarGame
        players[0] = new Player();
        players[1] = new Player();
        cargarImagenes();
        filtrarImagenes();
        gs.inicializar();
    }

    public void cargarImagenes() {
        //SUGERENCIA: cada pantalla o (estado de pantalla) deberia tener su
        //      propia funcion de cargar imagenes (y de filtrar imagenes), segun
        //      las que vaya a utilizar, sobre todo en juegos que posean un
        //      numero muy grande de imagenes
        try {
            imgPlayer[0] = cargar("player.GIF");
            imgPlayer[1] = cargar("player.GIF");
            imgEscenario = cargar("Escenario.GIF");
            imgKeyboard = cargar("keyboard.png");
            imgExplosiones = cargar("Explosiones.GIF");
            imgEnemyNoBlind[0] = cargar("EnemiesNoArmored.GIF");
            imgEnemyNoBlind[1] = cargar("EnemiesNoArmored.GIF");
            imgEnemyNoBlind[2] = cargar("EnemiesNoArmored.GIF");
            imgEnemyNoBlind[3] = cargar("EnemiesNoArmored.GIF");
            imgEnemyNoBlind[4] = cargar("EnemiesNoArmored.GIF");
            imgEnemyBlind[0] = cargar("EnemiesArmored.GIF");
            imgEnemyBlind[1] = cargar("EnemiesArmored.GIF");
            imgEnemyBlind[2] = cargar("EnemiesArmored.GIF");
            imgEnemyBlind[3] = cargar("EnemiesArmored.GIF");
            imgEnemyBlind[4] = cargar("EnemiesArmored.GIF");
            imgPresentando = cargar("presentando.GIF");
            imgCursor = cargar("cursor.GIF");
        } catch (Exception e) {
            System.out.println("No se encontraron las imagenes");
        }
    }

    public Image cargar(String str){
        ImageIcon img = new ImageIcon(getClass().getResource("imgs/"+str));
        return img.getImage();
    }

    public void filtrarImagenes() {
        imgPlayer[0] = gi.setImg(imgPlayer[0], gi.AMARILLO, false);
        imgPlayer[1] = gi.setImg(imgPlayer[1], gi.VERDE, false);
        imgEscenario = gi.setImg(imgEscenario, gi.COLOR_ORIGINAL, false);
        imgExplosiones = gi.setImg(imgExplosiones, gi.COLOR_ORIGINAL, true);
        imgEnemyNoBlind[0] = gi.setImg(imgEnemyNoBlind[0], gi.ROJO, false);
        imgEnemyNoBlind[1] = gi.setImg(imgEnemyNoBlind[1], gi.METAL, false);
        imgEnemyNoBlind[2] = gi.setImg(imgEnemyNoBlind[2], gi.CELESTE, false);
        imgEnemyNoBlind[3] = gi.setImg(imgEnemyNoBlind[3], gi.AZUL, false);
        imgEnemyNoBlind[4] = gi.setImg(imgEnemyNoBlind[4], gi.COLOR_ORIGINAL, false);
        imgEnemyBlind[0] = gi.setImg(imgEnemyBlind[0], gi.ROJO, false);
        imgEnemyBlind[1] = gi.setImg(imgEnemyBlind[1], gi.METAL, false);
        imgEnemyBlind[2] = gi.setImg(imgEnemyBlind[2], gi.CELESTE, false);
        imgEnemyBlind[3] = gi.setImg(imgEnemyBlind[3], gi.AZUL, false);
        imgEnemyBlind[4] = gi.setImg(imgEnemyBlind[4], gi.COLOR_ORIGINAL, false);
        imgPresentando = gi.setImg(imgPresentando, gi.COLOR_ORIGINAL, false);
        imgCursor = gi.setImg(imgCursor, gi.METAL, false);
    }

    public void keyTyped(KeyEvent e) {
        //nothing
    }

    public void keyPressed(KeyEvent e) {
        
        int p1 = 0;
        int p2 = 1;
        
        if(onNet){
            p1 = idPlayer-1;
            p2 = p1;
        }
        
        switch (e.getKeyCode()) {
            case KeyEvent.VK_NUMPAD1:
            case KeyEvent.VK_O:
                players[p1].cm.buttonA = true;
                players[p1].cmExtra.buttonA = true;
                break;
            case KeyEvent.VK_NUMPAD2:
            case KeyEvent.VK_P:
                players[p1].cm.buttonB = true;
                players[p1].cmExtra.buttonB = true;
                break;
            case KeyEvent.VK_UP:
                players[p1].cm.up = true;
                players[p1].cmExtra.up = true;
                break;
            case KeyEvent.VK_DOWN:
                players[p1].cm.down = true;
                players[p1].cmExtra.down = true;
                break;
            case KeyEvent.VK_LEFT:
                players[p1].cm.left = true;
                players[p1].cmExtra.left = true;
                break;
            case KeyEvent.VK_RIGHT:
                players[p1].cm.right = true;
                players[p1].cmExtra.right = true;
                break;
            case KeyEvent.VK_ENTER:
                players[p1].cm.start = true;
                players[p1].cmExtra.start = true;
                break;
        }
        switch (e.getKeyCode()) {
            case KeyEvent.VK_F:
                players[p2].cm.buttonA = true;
                players[p2].cmExtra.buttonA = true;
                break;
            case KeyEvent.VK_G:
                players[p2].cm.buttonB = true;
                players[p2].cmExtra.buttonB = true;
                break;
            case KeyEvent.VK_W:
                players[p2].cm.up = true;
                players[p2].cmExtra.up = true;
                break;
            case KeyEvent.VK_S:
                players[p2].cm.down = true;
                players[p2].cmExtra.down = true;
                break;
            case KeyEvent.VK_A:
                players[p2].cm.left = true;
                players[p2].cmExtra.left = true;
                break;
            case KeyEvent.VK_D:
                players[p2].cm.right = true;
                players[p2].cmExtra.right = true;
                break;
            case KeyEvent.VK_H:
                players[p2].cm.start = true;
                players[p2].cmExtra.start = true;
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
        int p1 = 0;
        int p2 = 1;
        
        if(onNet){
            p1 = idPlayer-1;
            p2 = p1;
        }
        
        switch (e.getKeyCode()) {
            case KeyEvent.VK_NUMPAD1:
                players[p1].cm.buttonA = false;
                players[p1].cmExtra.buttonA = false;
                break;
            case KeyEvent.VK_NUMPAD2:
                players[p1].cm.buttonB = false;
                players[p1].cmExtra.buttonB = false;
                break;
            case KeyEvent.VK_UP:
                players[p1].cm.up = false;
                players[p1].cmExtra.up = false;
                break;
            case KeyEvent.VK_DOWN:
                players[p1].cm.down = false;
                players[p1].cmExtra.down = false;
                break;
            case KeyEvent.VK_LEFT:
                players[p1].cm.left = false;
                players[p1].cmExtra.left = false;
                break;
            case KeyEvent.VK_RIGHT:
                players[p1].cm.right = false;
                players[p1].cmExtra.right = false;
                break;
            case KeyEvent.VK_ENTER:
                players[p1].cm.start = false;
                players[p1].cmExtra.start = false;
                break;
        }
        switch (e.getKeyCode()) {
            case KeyEvent.VK_F:
                players[p2].cm.buttonA = false;
                players[p2].cmExtra.buttonA = false;
                break;
            case KeyEvent.VK_G:
                players[p2].cm.buttonB = false;
                players[p2].cmExtra.buttonB = false;
                break;
            case KeyEvent.VK_W:
                players[p2].cm.up = false;
                players[p2].cmExtra.up = false;
                break;
            case KeyEvent.VK_S:
                players[p2].cm.down = false;
                players[p2].cmExtra.down = false;
                break;
            case KeyEvent.VK_A:
                players[p2].cm.left = false;
                players[p2].cmExtra.left = false;
                break;
            case KeyEvent.VK_D:
                players[p2].cm.right = false;
                players[p2].cmExtra.right = false;
                break;
            case KeyEvent.VK_H:
                players[p2].cm.start = false;
                players[p2].cmExtra.start = false;
                break;
        }
    }

    private void drawMinitank(Graphics g, int tipo, int px, int py) {
        Image img = null;
        int x  = 0;
        int y  = 0;
        int dx = 16;
        int dy = 16;
        img=imgEnemyNoBlind[1];
        switch(tipo){
            case 0:break;
            case 1:y=16;break;
            case 2:y=32;break;
            case 3:img=imgEnemyBlind[1];break;    
        }
        g.drawImage(img, (650 + px * 20)*kx/4, (280 + py * 40)*ky/3, (650 + px * 20)*kx/4 + 25*kx/4, (280 + py * 40)*ky/3 + 15*ky/3, x,y,x+dx,y+dy,null);
    }

    

    

    
}//end class
