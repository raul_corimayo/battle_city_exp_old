package JarGame2expansion;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class GestorSonido {
//Atributos-------------------------------------------------------------------//
    private String[] vecRutas ;
    private Clip fondo;
    public final int EXP_BALA = 0;
    public final int EXP_LADR = 1;
    public final int EXP_TANK = 2;
    public final int PROBLEM_0 = 3;
    public final int PROBLEM_1 = 4;
    public final int PROBLEM_2 = 5;
    public final int PROBLEM_3 = 6;
    public final int GAME_OVER = 7;
    public final int PRESENTAC = 8;
    public final int COIN = 9;
    public final int CAMPANA = 10;
    public final int STEP = 11;
    public final int SELECTION = 12;
    public final int ITEM = 13;
    public final int LIFE = 14;
    public final int WIN = 15;
    public final int LOSE = 16;
    public final int EXP_PIEDRA = 17;
    public final int EXP_SUPERBALA = 18;
//Metodos---------------------------------------------------------------------//
    public void inicializar(){
        String carpeta = "sounds/";
        String[] aux1 = {
            carpeta+"explosionBala.wav",//0
            carpeta+"explosionLadrillo.wav",//1
            carpeta+"explosionTank.wav",//2
            carpeta+"problem0.wav",//3
            carpeta+"problem1.wav",//4
            carpeta+"problem2.wav",//5
            carpeta+"problem3.wav",//6
            carpeta+"gameOver.wav",//7
            carpeta+"presentacion.wav",//8
            carpeta+"coin4.wav",//9
            carpeta+"campana.wav",//10
            carpeta+"coin3.wav",//11
            carpeta+"selection.wav",//12
            carpeta+"item.wav",//13
            carpeta+"life.wav",//14
            carpeta+"stageWin.wav",//15
            carpeta+"stageLose.wav",//16
            carpeta+"explosionPiedra.wav",//17
            carpeta+"explosionSuperBala.wav",//18
        };
        vecRutas = aux1;
    }
    public void play(int tipoSonido) {
        if(tipoSonido == new GestorSonido().EXP_BALA){
            return;
        }
        
        Hilo h=new Hilo("Hilo",vecRutas[tipoSonido]);
        //h.setPriority(h.MIN_PRIORITY);
        h.start();
    }
    public void playFondo(int tipoSonido){
        InputStream path;
        path = getClass().getResourceAsStream(vecRutas[tipoSonido]);
        try{
            InputStream bufferedIn = new BufferedInputStream(path);
            AudioInputStream audioStream =  AudioSystem.getAudioInputStream(bufferedIn);
            fondo = AudioSystem.getClip();
            fondo.open(audioStream);
            fondo.loop(fondo.LOOP_CONTINUOUSLY);
            fondo.start();
        }catch(Exception fallo){
            System.out.println(fallo);
        }
    }
    public void stopFondo(){
        fondo.stop();
        fondo.close();
    }
    
    
}//end class
