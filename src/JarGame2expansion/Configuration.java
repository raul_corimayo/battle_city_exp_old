
package JarGame2expansion;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JTextField;


public class Configuration extends JComponent{

    private int size = 2;
    private boolean left = false;
    private boolean right = false;
    private boolean start = false;

    
    public void main(){
        JTextField jt = new JTextField();
        this.add(jt);
        jt.addKeyListener(new java.awt.event.KeyListener() {
            public void keyTyped(KeyEvent e) {}
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyCode()){
                    case KeyEvent.VK_LEFT: left = true;break;
                    case KeyEvent.VK_RIGHT: right = true;break;
                    case KeyEvent.VK_ENTER: start = true;break;
                }
            }
            public void keyReleased(KeyEvent e) {
                switch(e.getKeyCode()){
                    case KeyEvent.VK_LEFT: left = false;break;
                    case KeyEvent.VK_RIGHT: right = false;break;
                    case KeyEvent.VK_ENTER: start = false;break;
                }
            }
        });
        while(!start){
            if(left){
                if(1<size){
                    size = size - 1;
                }
            }
            if(right){
                if(size<3){
                    size = size + 1;
                }
            }
            repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(Configuration.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    public void paint(Graphics g){
        int x0 = 90;
        int y0 = 200;
        int d = 30;



        for(int i=0;i<350;i++){
            g.setColor(new Color(30,40+i/2,40+i/2));
            g.drawLine(0, i, 400, i);
        }

        g.setColor(new Color(0,255,255));
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        g.drawString("Battle City ", x0, 60);
        g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 25));
        g.drawString("version REC ", x0, 90);
        g.drawString("super ", x0, 20);


        g.setColor(Color.BLACK);
        Font font1 = new Font(Font.MONOSPACED, Font.BOLD, 20);
        g.setFont(font1);

        for(int i=1;i<4;i++){
            g.drawString(""+i, x0+120+i*20, y0+30);
        }
        g.drawString("SIZE ", x0+40, y0+30);

        g.drawString("_", x0+120+size*20, y0+30);

        Font font2 = new Font(Font.MONOSPACED, Font.BOLD, 13);
        g.setFont(font2);

        g.drawString("use left-right to select size", x0, y0-d*3+16);
        g.drawString("press enter into ACCEPT", x0, y0-d*3+32);
        g.drawString("width = "+325*size+" , height = "+233*size, x0, y0-3*d+48);

    }

    public int getKX(){return size;}
    public int getKY(){return size;}
}
