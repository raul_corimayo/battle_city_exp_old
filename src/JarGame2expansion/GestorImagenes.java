package JarGame2expansion;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;

public class GestorImagenes {
//Atributos-------------------------------------------------------------------//
    private final int[][] MAT_INT_RGB_VERDE = {{153,204,153},{102,153,102},{51,102,51},{0,51,0}};
    private final int[][] MAT_INT_RGB_AMARILLO = {{255,255,102},{204,204,51},{128,128,0},{51,51,0}};
    private final int[][] MAT_INT_RGB_METAL = {{153,204,204},{102,153,153},{51,102,102},{0,51,51}};
    private final int[][] MAT_INT_RGB_ROJO = {{255,0,0},{153,0,0},{102,0,0},{51,0,0}};
    private final int[][] MAT_INT_RGB_ROSA = {{255,204,204},{255,153,153},{255,102,102},{153,51,51}};
    private final int[][] MAT_INT_RGB_CELESTE = {{153,204,204},{51,153,204},{51,102,153},{51,51,102}};
    private final int[][] MAT_INT_RGB_AZUL = {{51,51,255},{0,0,255},{0,0,128},{0,0,102}};
    private final int[][] MAT_INT_RGB_GRIS = {{192,192,192},{128,128,128},{102,102,102},{51,51,51}};
    public final int COLOR_ORIGINAL = -1;
    public final int VERDE = 0;
    public final int AMARILLO = 1;
    public final int METAL = 2;
    public final int ROJO = 3;
    public final int ROSA = 4;
    public final int CELESTE= 5;
    public final int AZUL = 6;
    public final int GRIS = 7;
//Metodos Públicos------------------------------------------------------------//
    public Image setImg (Image img, int color, boolean alpha){
        if(color!=COLOR_ORIGINAL){
            img = this.reemplazarColores(img, colores(GRIS), colores(color), 4);
        }
        if(alpha){
            //img = this.alpha(img, Color.black);
            //img = this.alpha2(img, Color.black, 0.5f);
            img = this.luz(img);
        }else{
            img = this.transp(img, Color.BLACK);
        }
        return img;
    }
//Metodos Privados------------------------------------------------------------//
    private Color[] colores (int color){
        Color[] aux = null;
        switch(color){
            case VERDE:     aux = this.colorInts(MAT_INT_RGB_VERDE);    break;
            case AMARILLO:  aux = this.colorInts(MAT_INT_RGB_AMARILLO); break;
            case METAL:     aux = this.colorInts(MAT_INT_RGB_METAL);    break;
            case ROJO:      aux = this.colorInts(MAT_INT_RGB_ROJO);     break;
            case ROSA:      aux = this.colorInts(MAT_INT_RGB_ROSA);     break;
            case CELESTE:   aux = this.colorInts(MAT_INT_RGB_CELESTE);  break;
            case AZUL:      aux = this.colorInts(MAT_INT_RGB_AZUL);     break;
            case GRIS:      aux = this.colorInts(MAT_INT_RGB_GRIS);     break;
        }
        return aux;
    }
    private Image transp(Image image, final Color cTransp) {
        //return rgb & 0x888888FF;//transparente 50%
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                if(rgb==cTransp.getRGB()){
                    return rgb & 0xFFFFFF;
                }
                return rgb;
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
    private Image alpha(Image image, final Color cTransp) {
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                if(rgb==cTransp.getRGB()){
                    return rgb & 0xFFFFFF;
                }
                int r = (rgb & 0xFF0000) >> 16;
                int g = (rgb & 0xFF00) >> 8;
                int b = rgb & 0xFF;
                Color color = new Color(r,g,b,126);// 126/256 = 50%
                return color.getRGB();
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
    private Image reemplazarColores(Image image, final Color[] cOrigen, final Color[] cDestino, final int n) {
        //return rgb & 0x888888FF;//transparente 50%
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                for(int i=0;i<n;i++){
                    if(cOrigen[i].getRGB()==rgb){
                        return cDestino[i].getRGB();
                    }
                }
                return rgb;
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
    private Color[] colorInts (int[][] v){//convierte valores enteros (RGB: 0-255) en Colores
        Color[] aux = new Color[v.length];
        for(int i=0;i<v.length;i++){
            aux[i] = new Color(v[i][0],v[i][1],v[i][2]);
        }
        return aux;
    }
    //ESTAs FUNCIONes SIRVEn :
    private Image alpha2(Image image, final Color cTransp, float alpha) {
        final int a = (int) (alpha * 255);
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                if(rgb==cTransp.getRGB()){
                    return rgb & 0xFFFFFF;
                }
                int r = (rgb & 0xFF0000) >> 16;
                int g = (rgb & 0xFF00) >> 8;
                int b = rgb & 0xFF;
                Color color = new Color(r,g,b,a);
                return color.getRGB();
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
    private Image luz(Image image) {
        ImageFilter filter = new RGBImageFilter() {
            public final int filterRGB(int x, int y, int rgb) {
                int r = (rgb & 0xFF0000) >> 16;
                int g = (rgb & 0xFF00) >> 8;
                int b = rgb & 0xFF;
                int a = (r+g+b)/3;//(r*0.25+g*0.40+b*0.35)
                Color color = new Color(r,g,b,a);
                return color.getRGB();
            }
        };
        ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }
}//end class
