package JarGame2expansion;

public class GestionDañoLadrillos {
//Sub Clase--------------------------------------------------------------------//
        class Ladrillo {
        //Atributos
            byte x;
            byte y;
            boolean[] c;
        //Métodos
            public Ladrillo(byte x, byte y, boolean[] c) {
                this.x = x;
                this.y = y;
                this.c = c;
            }
        }//end subclass
//Atributos--------------------------------------------------------------------//
    Ladrillo[] vectorLadrillos ;
    //item resguardo
    int itemTimeEfect;
    boolean palaActivado = false;
//Métodos----------------------------------------------------------------------//
    public int contarLadrillos(byte[][] matriz){
        //*recibe como parametro matriz que es de [dim][dim]
        //**codigo del ladrillo en matriz es = 1
        int aux=0;
        int matrizDimension = matriz.length;
        for(int i=0;i<matrizDimension;i++){//*
            for(int j=0;j<matrizDimension;j++){//*
                if(matriz[i][j]==1){//**
                    aux++;
                }
            }
        }
        return aux;
    }
    public void crearVector(int n){
        vectorLadrillos = new Ladrillo[n];//en inicializar(): n = nLadrillos + 8;
    }
    public void llenarVector(byte[][] matriz){
        int pos=0;
        int matrizDimension = matriz.length;
        for(byte i=0;i<matrizDimension;i++){
            for(byte j=0;j<matrizDimension;j++){
                if(matriz[i][j]==1){
                    boolean[] ladrilloCompleto = {true,true,true,true};
                    vectorLadrillos[pos]= new Ladrillo(i,j,ladrilloCompleto);//*
                    pos++;
                }
            }
        }
        //inicializar los espacios adicionales para el resguardo del aguila
        while(pos<vectorLadrillos.length){
            vectorLadrillos[pos]=null;
            pos++;
        }
    }
    public int posLadrillo (int x, int y) {
    //Determina la posicion de un ladrillo en el vectorLadrillos,
    //dando su posicion (x,y) en la matriz
        boolean encontrado = false;
        int pos = 0;
        while(!encontrado && pos<vectorLadrillos.length){
            if(vectorLadrillos[pos]!=null){//permite recorrer un vectorLadrillos con posiciones nulas de por medio
                if(vectorLadrillos[pos].x==x && vectorLadrillos[pos].y==y){
                    encontrado = true;
                    pos--;//decrementa para que al salir pos++; de el valor correcto
                }
            }
            pos++;
        }
        if(!encontrado){
            return -1;// si no lo encuentra devuelve -1
        }
        return pos;
    }
    public void dañarLadrillo(int dir, int pos, byte[][] matriz){
    //daño por disparo de bala
        // determinar los c[i] de la primera capa(i1,i2), segunda capa(i3,i4)
        int i1=0, i2=0, i3=0, i4=0;
        switch(dir){//0:arriba, 1:abajo, 2:izquierda, 3:derecha
            case 0:i1=1;i2=0;i3=2;i4=3;break;
            case 1:i1=3;i2=2;i3=1;i4=0;break;
            case 2:i1=2;i2=0;i3=1;i4=3;break;
            case 3:i1=3;i2=1;i3=2;i4=0;break;
        }
        // NOTA: puede ocurrir que solo tenga 1ª capa, solo 2ª, o ambas
        //determinar nuevo estado
        if(vectorLadrillos[pos].c[i1] || vectorLadrillos[pos].c[i2]){// si existe 1ª capa
            vectorLadrillos[pos].c[i1] = false;//dejaran de existir ambos
            vectorLadrillos[pos].c[i2] = false;
            if(!vectorLadrillos[pos].c[i3] && !vectorLadrillos[pos].c[i4]){
                // si no tiene 2ª capa --->eliminar
                eliminarLadrillo(matriz, pos);
            }
        }else{//dañar 2ª capa y eliminar
            vectorLadrillos[pos].c[i3] = false;//dejaran de existir ambos
            vectorLadrillos[pos].c[i4] = false;
            eliminarLadrillo(matriz, pos);
        }
    }
    public void eliminarLadrillo(byte[][] matriz, int pos){
        if(pos!=-1){
            matriz[vectorLadrillos[pos].x][vectorLadrillos[pos].y] = 0;
            vectorLadrillos[pos]=null;
        }
    }
    public void resguardoDeLadrillo(byte[][] matriz){
        byte[] x = {11,11,11,14,14,14,12,13};
        byte[] y = {23,24,25,23,24,25,23,23};
        int i = 0;//indice de posicion de ladrillos de reseguardo
        int pos = 0;//indice para recorrer el vectorLadrillos
        while(i<x.length && pos<vectorLadrillos.length){
            while(vectorLadrillos[pos]!=null && pos<vectorLadrillos.length){
                pos++;
            }//pos queda "apuntando" a la posicion null
            if(vectorLadrillos[pos]==null){
                boolean[] ladrilloCompleto = {true,true,true,true};
                vectorLadrillos[pos] = new Ladrillo(x[i],y[i],ladrilloCompleto);
                i++;
            }
            pos++;//seguir buscando posiciones vacias (null), a partir de la siguiente pos
        }
        construirResguardo(matriz, 1);//1:ladrillo
    }
    public void resguardoDeHierro(byte[][] matriz){
        eliminarResguardoLadrillos(matriz);
        construirResguardo(matriz, 2);//2:hierro
    }
    public void resguardoVacio(byte[][] matriz){
    // para cuando los enemigos capturen un item pala
        eliminarResguardoLadrillos(matriz);
        construirResguardo(matriz, 0);//0:nada == vacio
    }
    public void eliminarResguardoLadrillos(byte[][] matriz){
        eliminarLadrillo(matriz, posLadrillo(11,23));
        eliminarLadrillo(matriz, posLadrillo(11,24));
        eliminarLadrillo(matriz, posLadrillo(11,25));
        eliminarLadrillo(matriz, posLadrillo(14,23));
        eliminarLadrillo(matriz, posLadrillo(14,24));
        eliminarLadrillo(matriz, posLadrillo(14,25));
        eliminarLadrillo(matriz, posLadrillo(12,23));
        eliminarLadrillo(matriz, posLadrillo(13,23));
    }
    public void construirResguardo(byte[][] matriz, int r){
        byte rr = (byte)r;
        matriz[11][23]= rr;
        matriz[11][24]=rr;
        matriz[11][25]=rr;
        matriz[14][23]=rr;
        matriz[14][24]=rr;
        matriz[14][25]=rr;
        matriz[12][23]=rr;
        matriz[13][23]=rr;
    }
    public void efectoItemPala(byte[][] matriz, int itemTimeEfect, boolean isPlayer){
        if(isPlayer){
            resguardoDeHierro(matriz);
        }else{
            resguardoVacio(matriz);
        }
        palaActivado = true;
        this.itemTimeEfect = itemTimeEfect;
    }
    public void gestionPala(byte[][] matriz) {
        if(palaActivado){
            if(0<itemTimeEfect){
                itemTimeEfect--;
            }else{
                resguardoDeLadrillo(matriz);
                palaActivado = false;
            }
        }
    }
    //interfaces para las super clases
    public void inicializar (byte[][] matriz) {
        int numLadr = contarLadrillos(matriz);
        crearVector(numLadr+8);//8 para el resguardo que podria crearse a futuro
        llenarVector(matriz);
    }
}//end class