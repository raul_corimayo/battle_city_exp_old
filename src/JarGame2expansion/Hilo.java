package JarGame2expansion;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


public class Hilo extends Thread{
    String ruta;
    public Hilo(String str, String ruta){
        super(str);
        this.ruta = ruta;
    }

    
    @Override
    public void run(){
        InputStream path;
        path = getClass().getResourceAsStream(ruta);
        try{
//            Clip sonido = AudioSystem.getClip();
//            sonido.open(AudioSystem.getAudioInputStream(path));
           
            
            InputStream bufferedIn = new BufferedInputStream(path);
            AudioInputStream audioStream =  AudioSystem.getAudioInputStream(bufferedIn);

            Clip sonido = AudioSystem.getClip();
            sonido.open(audioStream);
            
    
            sonido.start();
            Thread.sleep(100);
            while (sonido.isRunning()){
                Thread.sleep(500);
            }
            sonido.close();
        }catch(Exception fallo){
            System.out.println(fallo);
        }
    }
}

