
package JarGame2expansion.net;

import java.util.ArrayList;

/**
 *
 * @author Raul
 */
public class StatusBuffer {
    
    private final ArrayList<GameStatusPacket> list = new ArrayList<GameStatusPacket>();
    public static int CAPACIDAD_MAXIMA = 2;
    
    public void meter(GameStatusPacket status){
        list.add(status);
    }
    
    public GameStatusPacket sacar(){
        GameStatusPacket status = list.get(0);
        list.remove(0);
        return status;
    }
    
    public int capacidad(){
        return list.size();
    }
    
    public boolean lleno(){
        return  capacidad() >= CAPACIDAD_MAXIMA;
    }
    
    
    
}
