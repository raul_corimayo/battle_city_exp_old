
package JarGame2expansion.net;

import JarGame2expansion.ControlMando;
import JarGame2expansion.Enemies.Item;
import java.io.Serializable;

/**
 *
 * @author Raul
 */
public class GameStatusPacket implements Serializable{
    
    public int idPlayer = -1;
    public ControlMando cm1;
    public ControlMando cm2;
    public ControlMando[] cme;
    public Item[] ic;
    
}
