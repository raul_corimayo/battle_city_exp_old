/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JarGame2expansion.net;

import JarGame2expansion.ControlMando;
import JarGame2expansion.Enemies;
import JarGame2expansion.Player;
import JarGame2expansion.Tanque;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gnet.client.ClientEventListener;
import org.gnet.client.GNetClient;
import org.gnet.client.ServerModel;
import org.gnet.packet.Packet;

/**
 *
 * @author Raul
 */
public class ClientInstance extends GNetClient{
    
    public ServerModel serverModel;
    public boolean wait = true;
    public int idPlayer = -1;
    public StatusBuffer buffer = new StatusBuffer();
    public int sends = 0;
    public int received = 0;

    public ClientInstance(String host, int port) {
        super(host, port);
        setDebugging(false);
        addEventListener(new ClientEventListener() {
            @Override
            protected void clientConnected(ServerModel sm) {
                serverModel = sm;
            }
            @Override
            protected void clientDisconnected(ServerModel sm) {
                
            }
            @Override
            protected void packetReceived(ServerModel sm, Packet packet) {
                if(packet.getPacketName().equals("numPlayers")){
                    int numPlayers = (Integer) packet.getEntry("numPlayers");
                    if(idPlayer == -1){
                        idPlayer = numPlayers;
                    }
                    switch (numPlayers){
                        case 1:wait=true;break;
                        case 2:wait=false;break;
                        default:/*ERROR*/;
                    }
                    return;
                }
                if(packet.getPacketName().equals("status")){
                    GameStatusPacket status = (GameStatusPacket) packet.getEntry("status");
                    buffer.meter(status);
                    return;
                }
            }
            @Override protected void debugMessage(String string) {}
            @Override protected void errorMessage(String string) {}
        });
    }
    
    public void init(){
        bind();
        start();
    }

    public GameStatusPacket receiveStatus() {
        while(buffer.capacidad() == 0){
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientInstance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        received++;
        return buffer.sacar();
    }

    public void sendStatus(GameStatusPacket status) {
        
        while(sends-received >= StatusBuffer.CAPACIDAD_MAXIMA){
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientInstance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        Packet p = new Packet("status", 1);
        p.addEntry("status", status);
        serverModel.sendPacket(p);
        sends++;
    }

    
}
