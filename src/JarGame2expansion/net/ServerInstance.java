/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JarGame2expansion.net;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.gnet.packet.Packet;
import org.gnet.server.ClientModel;
import org.gnet.server.GNetServer;
import org.gnet.server.ServerEventListener;

/**
 *
 * @author Raul
 */
public class ServerInstance extends GNetServer implements Runnable{

    StatusBuffer[] statusReceived = new StatusBuffer[2];
    GameStatusPacket statusMerged;
    
    public ServerInstance(String host, int port) {
        super(host, port);
        setDebugging(false);
        statusReceived[0] = new StatusBuffer();
        statusReceived[1] = new StatusBuffer();
        addEventListener(new ServerEventListener() {
            @Override
            protected void clientConnected(ClientModel cm) {
                Packet p = new Packet("numPlayers", 1);
                p.addEntry("numPlayers", getOnlineClients());
                sendToAll(p);
            }
            @Override
            protected void clientDisconnected(ClientModel cm) {
                
            }
            @Override
            protected void packetReceived(ClientModel cm, Packet packet) {
                if(packet.getPacketName().equals("status")){
                    GameStatusPacket sr = (GameStatusPacket) packet.getEntry("status");
                    statusReceived[sr.idPlayer-1].meter(sr);
                    return;
                }
            }
            @Override protected void debugMessage(String string) {}
            @Override protected void errorMessage(String string) {}
        });
    }  
    
    public void init(){
        bind();
        start();
        //enableServerMonitor();
    }
    
    public boolean statusComplete(){
        return ( 
            (0 < statusReceived[0].capacidad())  &&  
            (0 < statusReceived[1].capacidad()) 
        );
    }
    
    public void mergeStatusReceived(GameStatusPacket s1, GameStatusPacket s2){
        statusMerged = null;
        statusMerged = new GameStatusPacket();
        statusMerged.cm1 = s1.cm1 ;
        statusMerged.cm2 = s2.cm2 ;
        statusMerged.cme = s1.cme ;
        statusMerged.ic  = s1.ic  ;
    }

    @Override
    public void run() {
        while(true){
            if(statusComplete()){
                mergeStatusReceived(statusReceived[0].sacar(), statusReceived[1].sacar());
                Packet packetToSend = new Packet("status", 1);
                packetToSend.addEntry("status", statusMerged);
                sendToAll(packetToSend);
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ServerInstance.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
