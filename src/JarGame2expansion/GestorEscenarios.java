package JarGame2expansion;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestorEscenarios {
//--Atributos-------------------------------------------------------------------//
    public Escenario escenario;
    //finales
    private final String ENC = "maps/map";
    private final String ENC_CUSTOM = "maps/custom/map";
    private final String EXT = ".sbcREC";
    //variables
    private BufferedReader br;
//--Métodos---------------------------------------------------------------------//

    GestorEscenarios(int matrizDimension) {

        escenario = new Escenario(matrizDimension);
        File d = new File("maps");
        if(!d.exists()){
            //crear estructura
            this.createDirectory("maps");
        }
        // custom map folder
        if(! new File("maps/custom").exists()){
            //crear estructura
            this.createDirectory("maps/custom");
        }
        //crea escenarios iniciales
        for(int i=1;i<25;i++){
            try {
                if (this.loadInternalEscenario(i)){
                    if(! new File(ENC+i+EXT).exists()){
                        this.saveEscenario(
                                i, 
                                escenario.maxEnemySimul, 
                                escenario.tpoEntreNacimiento, 
                                escenario.vectorTipoTanquesEnemy, 
                                escenario.matriz26
                        );
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(GestorEscenarios.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(GestorEscenarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean createDirectory(String nameDirectory){
        File cd = new File(nameDirectory);
        return cd.mkdir();
    }
    
    
    
    public void guardarSerializado(){
        try{
            ObjectOutputStream objout = new ObjectOutputStream(new FileOutputStream(ENC+""+10+""+EXT));
            Escenario obj = new Escenario(26);
            objout.writeObject(obj);
            
            ObjectInputStream objin = new ObjectInputStream(new FileInputStream(ENC+""+10+""+EXT));
            Escenario nn = (Escenario) objin.readObject();
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public boolean saveEscenario(int stage, byte maxES, byte time, byte[] tipos, byte[][] matriz) throws IOException {
        boolean aux = false;
        escenario.tpoEntreNacimiento = time;
        escenario.maxEnemySimul = maxES;
        escenario.vectorTipoTanquesEnemy = tipos;
        escenario.matriz26 = matriz;
        File f = new File(ENC + stage + EXT);
        if (!f.exists()) {   
            ObjectOutputStream objout = new ObjectOutputStream(new FileOutputStream(ENC+stage+EXT));
            objout.writeObject(escenario);
            System.out.println("salvado " + stage);
            aux = true;
        } else {
            aux = false;
            System.out.println("no salvado " + stage);
        }
        return aux;
    }

    public boolean saveCustomEscenario(int stage, byte maxES, byte time, byte[] tipos, byte[][] matriz) throws IOException {
        boolean aux = false;
        escenario.tpoEntreNacimiento = time;
        escenario.maxEnemySimul = maxES;
        escenario.vectorTipoTanquesEnemy = tipos;
        escenario.matriz26 = matriz;
        File f = new File(ENC_CUSTOM + stage + EXT);
        if (!f.exists()) {   
            ObjectOutputStream objout = new ObjectOutputStream(new FileOutputStream(ENC_CUSTOM+stage+EXT));
            objout.writeObject(escenario);
            System.out.println("salvado " + stage);
            aux = true;
        } else {
            aux = false;
            System.out.println("no salvado " + stage);
        }
        return aux;
    }
    
    public boolean loadEscenario(int stage, boolean custom) throws IOException, ClassNotFoundException {
        boolean aux = true;
        String enc = ENC;
        String auxOut = "";
        if(custom){
            enc = ENC_CUSTOM;
            auxOut = " (custom)";
        }
        File f = new File(enc+stage+EXT);
        if (f.exists()) {
            ObjectInputStream objin = new ObjectInputStream(new FileInputStream(enc+stage+EXT));
            escenario = (Escenario) objin.readObject();
            System.out.println("cargado con exito el stage  " + stage+ auxOut);
        } else {
            aux = false;
            System.out.println("no se puede cargar el stage  " + stage+ auxOut);
        }
        return aux;
    }
    
    public boolean loadInternalEscenario(int stage) throws IOException, ClassNotFoundException {
        boolean aux = false;
        try {
            ObjectInputStream objin = new ObjectInputStream(GestorEscenarios.class.
                    getResourceAsStream(ENC+stage+EXT));
            escenario = (Escenario) objin.readObject();
            aux = true;
        } catch (Exception ex) {
            
        }
        return aux;
    }

    public boolean loadEscenario2(int stage) throws IOException, ClassNotFoundException {
        boolean aux = false;
        File f = new File(ENC + stage + EXT);
        if (f.exists()) {
            int ln = 0;
            aux = true;
            escenario.maxEnemySimul = readLineNumber(ln, f);
            ln++;
            System.out.println(escenario.maxEnemySimul);

            escenario.tpoEntreNacimiento = readLineNumber(ln, f);
            ln++;
            System.out.println(escenario.tpoEntreNacimiento);

            System.out.print("{");
            for (int i = 0; i < escenario.maxEnemysForLevel; i++) {
                escenario.vectorTipoTanquesEnemy[i] = readLineNumber(ln, f);
                ln++;
                System.out.print(","+escenario.vectorTipoTanquesEnemy[i]);
            }
            System.out.println("}");

            System.out.print("{");
            int matrizDimension = escenario.matriz26.length;
            for (int i = 0; i < matrizDimension; i++) {
                System.out.print(",{");
                for (byte j = 0; j < matrizDimension; j++) {
                    escenario.matriz26[i][j] = readLineNumber(ln, f);
                    ln++;
                    System.out.print(","+escenario.matriz26[i][j]);
                }
                System.out.println("}");
            }
            System.out.print("}");

            System.out.println("cargado con exito el stage  " + stage);
        } else {
            aux = false;
            System.out.println("no se puede cargar el stage  " + stage);
        }
        return aux;
    }
    
   
    
    public int getMaxEnemySimul() {
        return escenario.maxEnemySimul;
    }

    public int getMaxEnemysForLevel() {
        return escenario.maxEnemysForLevel;
    }

    public int getTpoEntreNacimiento() {
        return escenario.tpoEntreNacimiento;
    }

    public byte[] getVectorTipoTanquesEnemy() {
        return escenario.vectorTipoTanquesEnemy;
    }

    public byte[][] getMatriz26() {
        return escenario.matriz26;
    }
    ////////////////////////////////////////////////////////////////////////////
    public void delete(String name){
        File f = new File(name);
        boolean del = f.delete();
        System.out.println("Se borró el archivo?"+del);
    }

    public boolean existeCustomMapa(int loadStage) {
        return new File(ENC_CUSTOM+loadStage+EXT).exists();
    }
    
    public void writeln(int obj, File f) {
        try {
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            pw.println(obj);
            pw.close();
        } catch (java.io.IOException e) {
            System.out.println("No se pudo escribir");
        }
    }
//    public void write(Object obj, File f){
//        try{
//            FileWriter fw = new FileWriter(f,true);
//            BufferedWriter bw = new BufferedWriter(fw);
//            PrintWriter pw = new PrintWriter(bw);
//            pw.print(obj);
//            pw.close();
//        }catch(java.io.IOException e){
//            System.out.println("No se pudo escribir");
//        }
//    }
//    public String readFirstLine(File f){
//        String aux = "";
//        try{
//            br = new BufferedReader(new FileReader(f));
//            String str;
//            if((str = br.readLine())!= null){
//                aux = aux + str;}
//            br.close();
//        }catch(java.io.IOException ioex){
//            System.out.println("No se pudo leer");
//        }
//        return aux;
//    }

    public byte readLineNumber(int nl, File f) {
        byte aux = -1;
        try {
            br = new BufferedReader(new FileReader(f));
            String str;
            for (int r = 0; r < nl; r++) {
                br.readLine();
            }
            if ((str = br.readLine()) != null) {
            }
            aux = (byte) Integer.parseInt(str);
            br.close();
        } catch (java.io.IOException ioex) {
            System.out.println("No se pudo leer");
        }
        return aux;
    }
//    public String readAll(File f){
//        String aux = "";
//        try{
//            br = new BufferedReader(new FileReader(f));
//            String str;
//            while((str = br.readLine())!= null)
//                aux =aux + str;
//            br.close();
//        }catch(java.io.IOException e){
//            System.out.println("No se pudo leer");
//        }
//        return aux;
//    }
//    public void cleanAll(File f){
//        try{
//            FileWriter fw = new FileWriter(f,false);
//            BufferedWriter bw = new BufferedWriter(fw);
//            PrintWriter pw = new PrintWriter(bw);
//            pw.print("");
//            pw.close();
//        }catch(java.io.IOException e){
//            System.out.println("No se pudo borrar");
//        }
//    }
//    public void info (File f){
//        System.out.println("------------------Info Archivo--------------------");
//        System.out.println("    Existe?:"+f.isFile());
//        System.out.println("    Tamaño (Bytes):"+f.length());
//        System.out.println("--------------------------------------------------");
//        System.out.println(f.delete());
//
//    }
//
//    public void infoDirectory(){//podria devolver la lista
//        if(d.isDirectory()){
//            System.out.println("    Archvos Contenidos");
//            String[] list = d.list();
//            for(int i=0;i<list.length;i++){
//                System.out.println("    *   "+list[i]);
//            }
//        }else{
//            System.out.println("ERROR.GE. no es directorio");
//        }
//    }
    ///////////////////////////////////////////////////////////////////////////

    
}//end class


