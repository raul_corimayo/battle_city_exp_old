
package JarGame2expansion;

public class GestionDañoPiedras {
//Atributos-------------------------------------------------------------------//
        //Sub Clase-----------------------------------------------------------//
        class Piedra {
        //Atributos
            byte x;
            byte y;
            byte piedraVida;
        //Métodos
            public Piedra(byte x, byte y, byte c) {
                this.x = x;
                this.y = y;
                this.piedraVida = c;
            }
        }//end subclass
    Piedra[] vectorPiedra;
//Métodos---------------------------------------------------------------------//
    public int contarPiedras(byte[][] matriz){
        //*recibe como parametro matriz que es de [dim][dim]
        //**codigo de la piedra en matriz es = 6
        int aux=0;
        int matrizDimension = matriz.length;
        for(int i=0;i<matrizDimension;i++){//*
            for(int j=0;j<matrizDimension;j++){//*
                if(matriz[i][j]==6){//**
                    aux++;
                }
            }
        }
        return aux;
    }
    public void crearVector(int n){
        vectorPiedra = new Piedra[n];
    }
    public void llenarVector(byte[][] matriz){
        int pos=0;
        int matrizDimension = matriz.length;
        for(int i=0;i<matrizDimension;i++){
            for(int j=0;j<matrizDimension;j++){
                if(matriz[i][j]==6){
                    vectorPiedra[pos]= new Piedra((byte)i,(byte)j,(byte)8);//*
                    pos++;
                }
            }
        }
    }
    public int posPiedra (int x, int y) {
    //Determina la posicion de una piedra en el vectorPiedra,
    //dando su posicion (x,y) en la matriz
        boolean encontrado = false;
        int pos = 0;
        while(!encontrado && pos<vectorPiedra.length){
            if(vectorPiedra[pos]!=null){//permite recorrer un vectorPiedra con posiciones nulas de por medio
                if(vectorPiedra[pos].x==x && vectorPiedra[pos].y==y){
                    encontrado = true;
                    pos--;//decrementa para que al salir pos++; de el valor correcto
                }
            }
            pos++;
        }
        if(!encontrado){
            return -1;// si no lo encuentra devuelve -1
        }
        return pos;
    }
    public boolean dañarPiedra(int pos, byte[][] matriz){
    //devuelve true si la piedra se destruye por completo
    //daño por disparo de bala
        boolean aux = false;
        if(pos!=-1){
            vectorPiedra[pos].piedraVida--;
            if(vectorPiedra[pos].piedraVida<1){
                eliminarPiedra(matriz, pos);
                aux = true;
            }
        }
        return aux;
    }
    public void eliminarPiedra(byte[][] matriz, int pos){
        if(pos!=-1){
            matriz[vectorPiedra[pos].x][vectorPiedra[pos].y] = 0;
            vectorPiedra[pos]=null;
        }
    }
    //interfaces para las super clases
    public void inicializar (byte[][] matriz) {
        int numPiedras = contarPiedras(matriz);
        crearVector(numPiedras);
        llenarVector(matriz);
    }
}//end class