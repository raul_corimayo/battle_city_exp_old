package JarGame2expansion;

import java.io.Serializable;

/**
 * 
 * @author Cori
 */
public class Player extends Tanque implements Serializable{
//Atributos-------------------------------------------------------------------//
    public int vidas = 3;
    public int score = 0;
    public final int MAX_TTANQUES_VENCIDOS = 4;//4 tipos de enemigos
    public int[] tTanquesVencidos = new int[MAX_TTANQUES_VENCIDOS];
    public ControlMando cmExtra = new ControlMando();

    public int level = 0;//level = 0,1,2,3
  //auxiliar IA
    int timerIA;
    int r;
  //efecto reloj
    int tiempoRestanteEfReloj = 0;
//Métodos---------------------------------------------------------------------//
    public void addTanqueVencidoTipo(int tipo){
    //agrega un tipo de tanque enemigo (int) a la cola de tTanquesVencidos(int[])
        if(tipo < MAX_TTANQUES_VENCIDOS){
            tTanquesVencidos[tipo]++;
        }else{
            System.err.println("ERROR.Player: tTanquesVencidos tipoT fuera de rango!");
        }
    }
    public boolean vivo(){
        return (-1<vidas);
    }
    public void vaciarTanquesVencidos(){
        for(int i=0;i<MAX_TTANQUES_VENCIDOS;i++){
            tTanquesVencidos[i] = 0;
        }
    }
    public void addScore(int sco){
    //agrega "sco" al score acumulado del player
        score = score + sco;
    }
    public void upGrade(){
        if(level<4){
            level++;
            setValuesLevel(level);
        }else{
            blindaje++;
        }
    }
    public void setValuesLevel(int level){
        switch(level){//level = nuevo level (0,1,2,3)
            //         vb,#b,A,B, blindaje
            case 0:asig(3,1,0,0,1);break;
            case 1:asig(4,2,0,0,2);break;
            case 2:asig(5,2,0,0,3);break;
            case 3:asig(6,3,0,1,4);break;
            case 4:asig(7,3,0,2,5);break;
        }

    }
    public void asig(int bv, int mvc, int dA, int dB, int bl){
        balaVeloc = bv;
        maxBalasEnCurso = mvc;
        disparoA = dA;
        disparoB = dB;
        blindaje = bl;
    }
    public void IA(Enemies enemys){
        cm.falseAll_menosStart();
        int dxMin = 999;
        int dyMin = 999;
        //obtener distancias minimas
        for(int ne=0;ne<enemys.vecEnem.length;ne++){
            if(enemys.vecEnem[ne]!=null){
                int dx = enemys.vecEnem[ne].px - px;
                int dy = enemys.vecEnem[ne].py - py;
                if(Math.abs(dx)<Math.abs(dxMin)){dxMin=dx;}
                if(Math.abs(dy)<Math.abs(dyMin)){dyMin=dy;}
            }
        }
        //dispara si esta cerca el enemigo sino gira hacia esa direccion[+-]
        if(dxMin<10 && -10<dxMin){
            if(dyMin<0){
                if(dir==0){
                    cm.buttonA=true;
                }else{
                    cm.up=true;
                }
            }else{
                if(dir==1){
                    cm.buttonA=true;
                }else{
                    cm.down=true;
                }
            }
        }
        if(dyMin<10 && -10<dyMin){
            if(dxMin<0){
                if(dir==2){
                    cm.buttonA=true;
                }else{
                    cm.left=true;
                }
            }else{
                if(dir==3){
                    cm.buttonA=true;
                }else{
                    cm.right=true;
                }
            }
        }
        //si no disparo-->mover
        if(!cm.buttonA && !cm.buttonB){
            if(timerIA == 0){
                timerIA = (int)(Math.random()*100);
                r = (int)(Math.random()*5);
            }else{
                switch(r){
                    case 1:cm.down=true;break;
                    case 2:cm.left=true;break;
                    case 3:cm.right=true;break;
                    default:cm.up=true;break;
                }
                timerIA--;
            }
//            switch((int)(Math.random()*30)){
//                case 0:cm.buttonA=true;break;
//                case 1:cm.buttonB=true;break;
//            }
        }
    }

}//end class
