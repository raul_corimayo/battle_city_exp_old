package JarGame2expansion;

import java.io.Serializable;

public class ControlMando implements Serializable{
//Atributos-------------------------------------------------------------------//
    public Boolean up = false;
    public Boolean down = false;
    public Boolean left = false;
    public Boolean right = false;
    public Boolean buttonA = false;
    public Boolean buttonB = false;
    public Boolean start = false;
//Metodos---------------------------------------------------------------------//
    public boolean teclaPulsada () {
        return ( up || down || left || right || buttonA || buttonB || start);
    }
    public int intDir(){
        if(up){return 0;}
        else if(down){return 1;}
        else if(left){return 2;}
        else if(right){return 3;}
        else {return -1;}
    }
    public void falseAll_menosStart(){
        up = false;
        down = false;
        left = false;
        right = false;
        buttonA = false;
        buttonB = false;
    }

    
}
