
package JarGame2expansion;

public class Explosiones {
//Atributos-------------------------------------------------------------------//
    class Explosion{
        int time ;
        int pex ;
        int pey ;
        int tipo ;//0: ExpChica , 1: ExpGrande, 2:ExpPiedra, 3:ExpSuperBala
    }
    final int nMax = 25;
    Explosion[] ex = new Explosion[nMax];

//Metodos---------------------------------------------------------------------//
    public void add(int tipoX, int x, int y){
        int p=0;
        while(p<nMax && ex[p]!=null){p++;}//Para que funcione debe estar en esete orden(&&): p<nMax && ex[p]!=null
        //busca una pos != de null en p
        if(p<nMax){
            ex[p] = new Explosion();
            ex[p].pex=x;
            ex[p].pey=y;
            ex[p].tipo=tipoX;
            ex[p].time=0;
        }else{
            System.out.println("Explosiones: vector ex lleno!");
        }
    }
    public int[] getValues(int n){
        //devuelve los valores para dibujar
        //actualiza su time, y se elimina si termina el time
        int[] aux ={0,0,0,0, 0,0,0,0};
        if(n>=nMax){
            System.err.println("Explosiones: indice fuera de rango");
            return aux;
        }
        int time2 = ex[n].time/2;
        if(ex[n].tipo==0){
            aux[0]=ex[n].pex-8;
            aux[1]=ex[n].pey-8;
            aux[2]=ex[n].pex+8;
            aux[3]=ex[n].pey+8;
            aux[4]=16*time2;
            aux[5]=0;
            aux[6]=16*time2+16;
            aux[7]=16;
            ex[n].time++;
            if(8<ex[n].time){ex[n]=null;}//eliminada
        }else if (ex[n].tipo==1){
            aux[0]=ex[n].pex-24;
            aux[1]=ex[n].pey-24;
            aux[2]=ex[n].pex+24;
            aux[3]=ex[n].pey+24;
            if(time2<3){
                aux[4]=48*time2;
                aux[5]=16;
                aux[6]=48*time2+48;
                aux[7]=64;
            }else{
                aux[4]=48*(time2-3);
                aux[5]=64;
                aux[6]=48*(time2-3)+48;
                aux[7]=112;
            }
            ex[n].time++;
            if(10<ex[n].time){ex[n]=null;}//eliminada
        }else if(ex[n].tipo==2){
            aux[0]=ex[n].pex-8;
            aux[1]=ex[n].pey-8;
            aux[2]=ex[n].pex+8;
            aux[3]=ex[n].pey+8;
            aux[4]=16*time2;
            aux[5]=112;
            aux[6]=16*time2+16;
            aux[7]=128;
            ex[n].time++;
            if(16<ex[n].time){ex[n]=null;}//eliminada
        }else if(ex[n].tipo==3){
            aux[0]=ex[n].pex-16;
            aux[1]=ex[n].pey-16;
            aux[2]=ex[n].pex+16;
            aux[3]=ex[n].pey+16;
            aux[4]=16*time2;
            aux[5]=128;
            aux[6]=16*time2+16;
            aux[7]=144;
            ex[n].time++;
            if(16<ex[n].time){ex[n]=null;}//eliminada
        }else{
            System.err.println("tipo de explosion fuera de rango");
        }
        return aux;
    }

}//end class
