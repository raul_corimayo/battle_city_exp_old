
package JarGame2expansion;

import JarGame2expansion.*;
import java.io.Serializable;

public class Tanque implements Serializable{
//Atributos-----------------------------------------//
    public class BalaEnCurso implements Serializable{
        public int px=0;
        public int py=0;
        public int dir=0;
        public int tipo=0;//tipo A=0 , tipo B=1
    }
    public byte tipo = 0;//tipo de tanque
    public int balaVeloc=3;
    public int vel=2;//velocidad de desplazamiento del tanque
    public int px=0;//en general (no en las matrices)
    public int py=0;//en general (no en las matrices)
    public int nBalasEnCurso=0;//actualmente en curso
    public int maxBalasEnCurso=1;
    public int blindaje = 1;//un tanque blindado seteara este valor a 4 (Ejemplo)
    public int dir=0;//direccion hacia donde esta mirando
            //0=arriba, 1=abajo, 2=izquierda, 3=derecha
    public int step=0;//secuencia de animacion = 0 ó 1
    BalaEnCurso[] balasEnCurso = new BalaEnCurso[3];
    public ControlMando cm = new ControlMando();
    public int timeRemainingNaciendoMax = 75;//tiempo hasta que nace
    public int timeRemainingNaciendo = timeRemainingNaciendoMax;//tiempo hasta que nace
    public boolean itemPortado = false;
    public int timeItem = 0;
    public int timeItemMax = 10;
    public boolean lightItem = false;
  //escudo
    public int escudoEstado = 0;
    public boolean escudoActivado = false;
    public int escudoTime = 0;
  //tipos de bala
    public int disparoA = 0;
    public int disparoB = 0;
//Metodos-----------------------------------------//
    
    public void dispararA (){
        dispararTipo(disparoA);
    }
    public void dispararB (){
        dispararTipo(disparoB);
    }
    public void dispararTipo(int tipo){
        if(nBalasEnCurso<maxBalasEnCurso){
            balasEnCurso[nBalasEnCurso] = new BalaEnCurso();
            balasEnCurso[nBalasEnCurso].dir=dir;
            balasEnCurso[nBalasEnCurso].px=px+6;
            balasEnCurso[nBalasEnCurso].py=py+6;
            balasEnCurso[nBalasEnCurso].tipo=tipo;
            nBalasEnCurso++;
        }
    }
    public void eliminarBala(int nb){
        if(0<nBalasEnCurso){
            for(int p=nb;p<nBalasEnCurso-1;p++){
                balasEnCurso[p]=balasEnCurso[p+1];
            }
            nBalasEnCurso--;
        }
    }
    public void dStep(){step=1-step;}
    public void dLigthItem(){
        if(timeItem<timeItemMax){
            timeItem++;
        }else{
            timeItem = 1;
            lightItem = !lightItem;
        }
    }
    public int indexTRN(){//indice de pose para tiempo restante de nacimiento
        int aux = -1;
        int k = 5;//duracion de cada estado
        int i = (int) Math.IEEEremainder(timeRemainingNaciendo, k*6);
        if(i<0){i=i+k*6;}
        i = i/k;
        switch(i){
            case 0:aux=3;break;
            case 1:aux=2;break;
            case 2:aux=1;break;
            case 3:aux=0;break;
            case 4:aux=1;break;
            case 5:aux=2;break;
        }
        if(aux==-1){System.err.println("ERROR.Tanque.indexTRN");}
        return aux;
    }
//escudo
    public void dEscudoEstado() {
        escudoEstado = 1-escudoEstado;
    }
    public void efectoItemEscudo(int time) {
        escudoActivado = true;
        escudoTime = time;
    }
    public void gestionarEscudo(){
        if(escudoActivado){
            if(0<escudoTime){
                escudoTime--;
            }else{
                escudoActivado = false;
            }
        }
    }
}//end class Tanque
