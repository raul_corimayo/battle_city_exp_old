package JarGame2expansion;

import java.io.Serializable;
import java.util.ArrayList;

public class Enemies implements Serializable{
//Atributos-------------------------------------------------------------------//
    public int maxEnemysSimul ;
    public int maxEnemysForLevel ;
    public Tanque[] vecEnem ;
    public byte[] vectorTipoTanquesEnemy ;
    public int nVTTE ;
    public int[] vectItemsNTT = {2,6,10,14,18,22,26,30,34,38,42,46,50,54,58,62,66,70,74,
            78,82,86,90,94,98};//contiene numero de TT (ordenados) que portan items
    public int nextPosicEnemy ;//0,1,2
    public int[] timerTank ;//auxiliar IA
    public int[] timer2Tank ;//auxiliar IA
    public int[] rTank ;//auxiliar IA
    public boolean[] iaTestTank ;//auxiliar IA
    public int tpoEntreNacimiento ;//valor inicial
    public int countTpoEntreNac ;
    public boolean congelarIA = false;
//**item**//
    public int nMaxItem = vectItemsNTT.length;
    public class Item implements Serializable{
        int x = 0;//posicion x del item
        int y = 0;//posicion y del item
        int id = 0;
    }
    public Item[] item = new Item[nMaxItem];
    public int[] itemTimeEfectMax = {500,500,500,500,500,500};//6 tipos de item
    public int[] itemTimeEfect = new int[itemTimeEfectMax.length];
/**/public boolean palaActivado = false;
    public boolean resguardoLadrillo = false;
    public boolean resguardoHierro = false;
    
    // net
    public ArrayList<Item> listaItemsCreados = new ArrayList<Item>();
    
    // IA difficulty general
    private int[] diff = {5,7,10,15,25};
//Metodos---------------------------------------------------------------------//
    public int[] posicNewEnemy (){
        int[] aux = {0,0};//[0]=x , [1]=y
        //calcula las coordenadas de la posicion
        aux[0]=nextPosicEnemy*96;
        aux[1]=0;
        //calcula la proxima posicion del enemigo
        if(nextPosicEnemy<2){
            nextPosicEnemy++;
        }else{
            nextPosicEnemy=0;
        }
        return aux;
    }
    public int[] posicNewItem(){
        int x = (int) (Math.random() * 12);
        int y = (int) (Math.random() * 12);
        int [] aux = {16*x+8, 16*y+8};
        return aux;
    }
    
    public void setValuesTank(int idT, int bv, int v, int mvc, int b){
        // asigna caracteristicas segun el tipo de tanque
        vecEnem[idT].balaVeloc = bv;
        vecEnem[idT].vel = v;
        vecEnem[idT].maxBalasEnCurso = mvc;
        vecEnem[idT].blindaje = b;
    }
    public int getScoreTankType(int type){
        return ((type+1)*100);
    }

    public int newIndexVecEnem(){
        //busca un lugar libre (null) en el vecEnem
        int aux = -1;//si no encuentra que da con -1 (significa no encontrado)
        boolean encontrado = false;
        int pos = 0;
        while(!encontrado && pos < vecEnem.length){
            if(vecEnem[pos] == null){
                aux = pos;
                encontrado = true;
            }
            pos++;
        }
        return aux;
    }
    public void newTank (int stage){
        int pos = newIndexVecEnem();
        if(pos != -1){// si hay lugar y si hay tanques
            int tipo = getNextTipo();//obtengo el tipo siguiente
            if(tipo != -1){
                vecEnem[pos] = new Tanque ();//creo un objeto tanque
                countTpoEntreNac = tpoEntreNacimiento;//tpoEntreNacimiento de espera para crear un nuevo tanque
                /*mejoras por stage */
                int dbl = stage/5;//aumento del blindaje en 1 cada 5 stages
                int dbv = stage/5;
                switch(tipo){//4 tipos de tanque
                    case 0:setValuesTank(pos,3+dbv,2,1,1+dbl);break;
                    case 1:setValuesTank(pos,3+dbv,3,1,1+dbl);break;
                    case 2:setValuesTank(pos,5+dbv,2,1,1+dbl);break;
                    case 3:setValuesTank(pos,2+dbv,1,1,4+dbl);break;
                }
                if(99<tipo){
                    tipo = tipo -100;//100==estrella agarrada para los enemigos que resucitan
                    vecEnem[pos].tipo = (byte) tipo;
                    upGrade(pos);
                }else{
                    vecEnem[pos].tipo = (byte) tipo;
                }
                int[] pne = posicNewEnemy();
                vecEnem[pos].px = pne[0];//pne.x
                vecEnem[pos].py = pne[1];//pne.y
                //porta item?
                boolean itemP = false;
                int i=0;
                while(i<vectItemsNTT.length && !itemP ){
                    if(nVTTE==vectItemsNTT[i]){
                        itemP = true;
                    }
                    i++;
                }
                if(itemP){
                    vecEnem[pos].itemPortado = true;
                }
            }
        }
    }
    public void deadTank(int id, boolean onNet){
        if(newIndexVecEnem()==-1){
            countTpoEntreNac = tpoEntreNacimiento;
        }
        if(vecEnem[id].itemPortado){
        //si el enemigo que muere porta un item --> crear item nuevo en el escenario
            if(onNet){
                Item itemNuevo = new Item();
                //int pos = newPosItem();
                int[] aux = posicNewItem();
                itemNuevo.id = (int) (Math.random()*6);//tipos de items = 6;
                itemNuevo.x = aux[0];
                itemNuevo.y = aux[1];
                listaItemsCreados.add(itemNuevo);
            }else{
                int pos = newPosItem();
                int[] aux = posicNewItem();
                item[pos] = new Item();
                item[pos].id = (int) (Math.random()*6);//tipos de items = 6;
                item[pos].x = aux[0];
                item[pos].y = aux[1];
            }
        }
        vecEnem[id] = null;
    }
    
    public void addItemNuevo(Item itemNuevo){
        int pos = newPosItem();
        item[pos] = itemNuevo;
    }
    
    public void vaciarListaItemsCreados(){
        listaItemsCreados.clear();
    }
    
    public int newPosItem(){
        int aux = -1;//si no lo encuentra devuelve -1
        // deberia encontrar lugar siempre por que hay tantos lugares
        // como tanques portadores de items
        boolean encontrado = false;
        int pos = 0;
        while(!encontrado && (pos<nMaxItem)){
            if(item[pos]==null){
                encontrado = true;
                aux = pos;
            }
            pos++;
        }
        return aux;
    }
    public int getNextTipo(){
        int aux = -1;//-1 significa que no hay siguiente (fin de la ED)
        if(nVTTE<vectorTipoTanquesEnemy.length){
            aux = vectorTipoTanquesEnemy[nVTTE];
            nVTTE++;
        }
        return aux;
    }
    public boolean endEnemys(){
        boolean aux = false;
        if(nVTTE>=vectorTipoTanquesEnemy.length){
            aux = true;
            for(int pos=0;pos<vecEnem.length;pos++){
                if(vecEnem[pos]!=null){
                    return false;
                }
            }
        }
        return aux;
    }
    public void IA(int id, Player[] players, int stage, int indexDifficulty) {//version de prueba
        if(!congelarIA){
          randomIA(id, players, stage, indexDifficulty);
          //testIA(id, players, stage, indexDifficulty);
        }
    }
    
    
    
    public void randomIA(int id, Player[] players, int stage, int indexDifficulty){
        if(iaTestTank == null){
            iaTestTank = new boolean[timerTank.length];
        }
        int r = (int)(Math.random()*diff[indexDifficulty]);//
        if(timerTank[id] == 0){
            vecEnem[id].cm.falseAll_menosStart();
            iaTestTank[id] = false;
            timerTank[id] = (int)(Math.random()*30);
            switch(r){
                case 0:vecEnem[id].cm.up=true;break;
                case 1:vecEnem[id].cm.down=true;break;
                case 2:vecEnem[id].cm.left=true;break;
                case 3:vecEnem[id].cm.right=true;break;
                case 4:/*nada*/break;
                default:iaTestTank[id]=true;break;//6,7
            }
        }else{
            timerTank[id]--;
            if(iaTestTank[id]){
                testIA(id, players, stage, indexDifficulty);
            }
        }
        int[] casos = {300,150,50,50,150};
        r = (int)(Math.random()*casos[indexDifficulty]);
        switch(r){
            case 0:vecEnem[id].cm.buttonA=true;break;
            case 1:vecEnem[id].cm.buttonB=true;break;
        }
    }
    
    public void testIA(int id, Player[] players, int stage, int indexDifficulty){
        int aguilaX = 12*8;
        int aguilaY = 24*8;
        //players[0].px = 0;
        //players[0].py = 0;
        //players[0].timeRemainingNaciendo = 0;
        //players[0].level = 0;
        //players[0].blindaje = 100;
        //int tdsf = vecEnem.length;
        
        //vecEnem[id].px;
        
        vecEnem[id].cm.falseAll_menosStart();
        
        Player player1 = players[0];
        Player player2 = players[1];
        
        int atacarAguila = 0;//
        int atacarPlayer1 = 1;//
        int atacarPlayer2 = 2;//
        int atacar = atacarAguila;
        
        if(player1 != null){
            if(0 == player2.timeRemainingNaciendo){
                atacar = atacarPlayer2;
            }
            if(0 == player1.timeRemainingNaciendo){
                atacar = atacarPlayer1;
            }
        }
        
        if(timer2Tank[id] == 0){
            rTank[id] = (int) (Math.random()*2);
            timer2Tank[id] = (int) (Math.random()*30);
        }else{
            timer2Tank[id]--;
        }
        
        boolean atacarHorizontal = false;
        boolean atacarVertical = false;
        int delta = 3;
        switch(atacar){
            case 0:
                //atacar aguila
                if(rTank[id]==0){
                    if(vecEnem[id].px < aguilaX -delta){
                        vecEnem[id].cm.right = true;
                    }else if(vecEnem[id].px > aguilaX +delta){
                        vecEnem[id].cm.left = true;
                    }else{
                        atacarVertical = true;
                    }
                }else{
                    if(vecEnem[id].py > aguilaY +delta){
                        vecEnem[id].cm.up = true;
                    }else if(vecEnem[id].py < aguilaY -delta){
                        vecEnem[id].cm.down = true;
                    }else{
                        atacarHorizontal = true;
                    }
                }
                break;
            case 1:
                //atacar player 1
                if(rTank[id]==0){
                    if(vecEnem[id].px < player1.px -delta){
                        vecEnem[id].cm.right = true;
                    }else if(vecEnem[id].px > player1.px +delta){
                        vecEnem[id].cm.left = true;
                    }else{
                        atacarHorizontal = true;
                    }
                }else{
                    if(vecEnem[id].py > player1.py +delta){
                        vecEnem[id].cm.up = true;
                    }else if(vecEnem[id].py < player1.py -delta){
                        vecEnem[id].cm.down = true;
                    }else{
                        atacarVertical = true;
                    }
                }
                break;
            case 2:
                //atacar player 2
                if(rTank[id]==0){
                    if(vecEnem[id].px < player2.px -delta){
                        vecEnem[id].cm.right = true;
                    }else if(vecEnem[id].px > player2.px +delta){
                        vecEnem[id].cm.left = true;
                    }else{
                        atacarHorizontal = true;
                    }
                }else{
                    if(vecEnem[id].py > player2.py +delta){
                        vecEnem[id].cm.up = true;
                    }else if(vecEnem[id].py < player2.py -delta){
                        vecEnem[id].cm.down = true;
                    }else{
                        atacarVertical = true;
                    }
                }
                break;
        }
        
        if(atacarHorizontal || atacarVertical){
            vecEnem[id].cm.falseAll_menosStart();
            
            int px = 0;
            int py = 0;
            switch(atacar){
                case 0:
                    px = aguilaX;
                    py = aguilaY;
                    break;
                case 1:
                    px = players[0].px;
                    py = players[0].py;
                    break;
                case 2:
                    px = players[1].px;
                    py = players[1].py;
                    break;
            }
            
            if(atacarHorizontal){
                if(py < vecEnem[id].py){
                    if(vecEnem[id].dir==1){
                        vecEnem[id].cm.buttonA=true;
                    }else{
                        vecEnem[id].cm.down=true;
                    }
                }else{
                    if(vecEnem[id].dir==0){
                        vecEnem[id].cm.buttonA=true;
                    }else{
                        vecEnem[id].cm.up=true;
                    }
                }
            }
            
            if(atacarVertical){
                if(px < vecEnem[id].px){
                    if(vecEnem[id].dir==2){
                        vecEnem[id].cm.buttonA=true;
                    }else{
                        vecEnem[id].cm.left=true;
                    }
                }else{
                    if(vecEnem[id].dir==3){
                        vecEnem[id].cm.buttonA=true;
                    }else{
                        vecEnem[id].cm.right=true;
                    }
                }
            }
            
        }
        
            
        
    }
    
    public void gestionarNacimientosYMuertes(int stage){
        if(countTpoEntreNac<0){
            newTank(stage);
        }
        countTpoEntreNac--;
    }
    public void efectoItemReloj(){
        congelarIA = true;
        for(int ne=0;ne<vecEnem.length;ne++){
            if(vecEnem[ne] != null){
                //ne = enemys (2,3,4,...)
                vecEnem[ne].cm.falseAll_menosStart();
            }
        }
    }
    public void gestionReloj(){
        if(congelarIA){
            if(0<itemTimeEfect[2]){
                itemTimeEfect[2]--;
            }else{
                congelarIA = false;
            }
        }
    }
    public void upGrade(int ne){
        vecEnem[ne].maxBalasEnCurso = 2;
        vecEnem[ne].disparoB = 1;
        if(2<vecEnem[ne].tipo){//tanque blindado
            vecEnem[ne].blindaje = 8;
            vecEnem[ne].itemPortado = true;
        }else{
            vecEnem[ne].blindaje = 4;
        }
    }
    public void vida(byte ne){
        byte cien = 100;
        nVTTE--;
//(!) buscar otra forma de detectar las mejoras:  "vecEnem[ne].disparoB==1"
        if(vecEnem[ne].disparoB==1){
            //tenia una estrella capturada
            vectorTipoTanquesEnemy[nVTTE] = (byte) (vecEnem[ne].tipo + cien);
        }else{
            vectorTipoTanquesEnemy[nVTTE] = vecEnem[ne].tipo;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    public ControlMando[] getVecMCs(){
        ControlMando[] cms = new ControlMando[vecEnem.length];
        for(int i=0;i<vecEnem.length;i++){
            if(vecEnem[i] != null){
                cms[i] = vecEnem[i].cm;
            }
        }
        return cms;
    }
    
    public void setVecMCs(ControlMando[] cms){
        for(int i=0;i<vecEnem.length;i++){
            if(vecEnem[i] != null  &&  cms[i] != null){
                vecEnem[i].cm = cms[i];
            }
        }
    }
}//end class
